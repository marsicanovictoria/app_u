
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

require('./components/Example');

/* Import the Main component */
import React, { Component } from 'react';
import Route from 'react-router-dom/Route'
import Administracion from '../Administracion'
import Main from '../Main'
import { BrowserRouter } from 'react-router-dom'
import Nav from "./NavBar"
class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <div>
            <Nav />
            <Route exact path="/" component={Main} />
            <Route path="/Administracion" component={Administracion} />
          </div>
        </BrowserRouter>
      </div>
    );
  }
}
export default App;