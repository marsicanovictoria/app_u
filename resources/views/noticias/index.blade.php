@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
         <h4 class="w3-center"></h4>
        <form method="GET" action="{{action('NoticiasController@search')}}"  role="form" enctype="multipart/form-data">
              {{ csrf_field() }}
         <p>Buscar por título</p>
         <p><input type="text" name="titulo" class="form-control input-sm"></p>
                         <div class="col-xs-12 col-sm-12 col-md-12">
                  <input type="submit"  value="Buscar" class="w3-button w3-theme-d2 w3-margin-bottom">
                  </div>
         </form>
         <p><i class="fa fa-birthday-cake fa-fw w3-margin-right w3-text-theme"></i></p>
        </div>

      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
          <button type="button" class="w3-button w3-block w3-theme-l4">
            <a href="{{  route('noticia.create') }}" class="w3-button-up" title="Messages">Nueva +</a></button>
          <button type="button" class="w3-button w3-block w3-theme-l4">
            <a href="{{  route('noticia.mis_noticias') }}" class="w3-button-up" title="Messages">Mis noticias</a></button>
          <button type="button" class="w3-button w3-block w3-theme-l4">
            <a href="{{  route('noticia.presentacion') }}" class="w3-button-up" title="Messages">Atrás</a></button>

        </div>
      </div>    

      </div>
      <br>   
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding .w3-padding-blog">
        @if($noticias->count())  
        @foreach($noticias as $noticia)
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container">
            <h2>{{ $noticia->  created_at->toFormattedDateString()}}</h2>
            <p><i style="color:#BDBDBD;">{{ $noticia-> nombre }}</i></p>
            <p><i style="color:#BDBDBD;">{{ $noticia-> email }}</i></p>
            <h2><i>{{ $noticia->  titulo}}</i></h2>


          
          <a href="{{action('NoticiasController@show', $noticia->id)}}" ><i style="color: blue;"> Ver más +</i></a>
<br>
            </div>
          </div>
             <br>
        </div>
    @endforeach 
    @endif

            <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container w3-padding">
  <div class="w3-content" style="max-width:700px">
    <p>Comentarios</p>
    
<form method="POST" action="{{action('NoticiasController@comentario')}}"  role="form">
              {{ csrf_field() }}
      <p><input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <p>Nombre</p>
        <p><input class="w3-input w3-padding-16 w3-border" type="text"  name="nombre" id="nombre" required value=""></p>
        <p>Apellido</p>
        <p><input class="w3-input w3-padding-16 w3-border" type="text"  name="apellido" id="apellido" required value=""></p>
        <p>Correo electrónico</p>
        <p><input class="w3-input w3-padding-16 w3-border" type="text"  name="email" id="email" required value=""></p>
        <p>Deja tu comentario</p>
        <input class="w3-input w3-padding-16 w3-border" type="text"  name="comentario" id="comentario" required value=""></p>
      <button class="w3-button w3-theme-d2 w3-margin-bottom" type="submit">Enviar</button></p>
    </form>

    </div>
            </div>
          </div>
        </div>
      </div>

         <br>
   
   @if($c->count())  
@foreach($c as $c)
      <div class="w3-row-padding">

        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container w3-padding">
  <div class="w3-content" style="max-width:700px">



  <p>{{ $c-> nombre}}</p>
  <p>{{ $c-> email}}</p>
  <p>{{ $c-> comentario}}</p>
  <p>{{ $c-> created_at}}</p>


    </div>
            </div>
          </div>
        </div>

      </div>
    </br>

            @endforeach 
@else
              <div>
                <p>No hay comentarios !!</p>
            
              @endif  
              </div>

      </div>
<br>

   
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
<div class="w3-container">
</div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>
<script type="text/javascript">
  var texto, padre;
$(".contenidoBlog").each(function(){
    texto = $(this).html();
    this.setAttribute("data-texto", texto);
    if ($(this).html().length > 75){
        $(this)
            .html(texto.substr(0, 75) + "...");
    }
});
 

</script>
@endsection
