@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
      <div class="w3-card w3-round w3-white">


      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
          
        </div>
      </div>    

      </div>
      <br>   
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding .w3-padding-blog">

      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Error!</strong> Revise los campos obligatorios.<br><br>
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if(Session::has('success'))
      <div class="alert alert-info">
        {{Session::get('success')}}
      </div>
      @endif
          <div class="table-container">
            <form method="POST" action="{{ route('noticia.store') }}"  role="form" enctype="multipart/form-data">
              {{ csrf_field() }}
 
              <div class="form-group">
                <textarea name="titulo" id="titulo" class="form-control input-sm" placeholder="Título"></textarea>
              </div>
              <div class="form-group">
                <textarea name="contenido" id="contenido" class="form-control input-sm" maxlength="3000" rows="10" placeholder="Contenido"></textarea>
              </div>
              <div class="form-group">
                <textarea name="nombre" id="nombre" class="form-control input-sm" placeholder="Nombre"></textarea></div>
              <div class="form-group">
                <textarea name="email" id="email" class="form-control input-sm" placeholder="E-mail"></textarea></div>

              <p>Tipo de identificación</p>

              {!! Form::select('identificacion', $identificacion, null, ['class' => 'form-control'] ) !!}
              <div class="form-group">
                    <textarea name="numero_identificacion" id="numero_identificacion" class="form-control input-sm" maxlength="20"  placeholder="Número de identificación"></textarea>
              </div>

              <br>
              <div class="form-group">
                    <textarea name="entidad" id="entidad" class="form-control input-sm" maxlength="100" placeholder="Nombre entidad"></textarea>
              </div>
              <div class="form-group">
                    <textarea name="carrera" id="carrera" class="form-control input-sm" maxlength="100" placeholder="Carrera"></textarea>
              </div>

            <div class="form-group">
                <img width="50px" src="/img/subir_imagen.png">
                <input type="file" name="imagen" id="noticiaImg" >
              </div>
              <div class="form-group">
               <img width="50px" src="/img/pdf.jpg">
                <input type="file" name="pdf" class="form-control input-sm"  >
              </div>
              <div class="form-group">
                <img width="50px" src="/img/ppt.jpg">
                <input type="file" name="ppt" class="form-control input-sm"  >
              </div>
              <div class="form-group">
                 <img width="50px" src="/img/word.png">
                <input type="file" name="word" class="form-control input-sm"  >
              </div>
              <div class="form-group">
                <img width="50px" src="/img/excel.png">
                <input type="file" name="excel" class="form-control input-sm"  >
              </div>
              <div class="form-group">
                <img width="50px" src="/img/video.jpg">
                <input type="file" name="video" class="form-control input-sm"  >
              </div>  
          <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
             <div class="captcha">
               <span>{!! captcha_img('flat') !!}</span>

               </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
             <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha"></div>
          </div>
 
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <input type="submit"  value="Guardar" class="btn btn-success btn-block">
                  <a href="{{ url('noticia.index') }}" class="btn btn-info btn-block" > Atrás</a></div>
                             
            </form>
          </div>

      </div>

<br>

   
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
<div class="w3-container">
</div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>

@endsection
<script type="text/javascript">
$('#refresh').click(function(){
  $.ajax({
     type:'GET',
     url:'refreshcaptcha',
     success:function(data){
        $(".captcha span").html(data.captcha);
     }
  });
});
</script>