@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
      <div class="w3-card w3-round w3-white">


      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
          
        </div>
      </div>    

      </div>
      <br>   
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding .w3-padding-blog">

      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Error!</strong> Revise los campos obligatorios.<br><br>
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if(Session::has('success'))
      <div class="alert alert-info">
        {{Session::get('success')}}
      </div>
      @endif
          <div class="table-container">
               <div class="form-group">
                Nombre
                <textarea name="nombre" id="nombre" class="form-control input-sm" placeholder="Nombre">{{$com->nombre}}</textarea>
              </div>
              <div class="form-group">
                Apellido
                <textarea name="email" id="email" class="form-control input-sm" placeholder="Email">{{$com->apellido}}</textarea>
              </div>
            
              <div class="form-group">
                Email
                <textarea name="numero_identificacion" id="numero_identificacion" class="form-control input-sm" placeholder="Número de identificación">{{$com->email}}</textarea>
              </div>
              <div class="form-group">
                Comentario
                <textarea name="titulo" id="titulo" class="form-control input-sm" maxlength="100" placeholder="Título">{{$com->comentario}}</textarea>
              </div>
              <div class="form-group">
                Fecha creacion
                    <textarea name="contenido" id="contenido" class="form-control input-sm" maxlength="2000" rows="5" placeholder="Contenido">{{$com->created_at}}</textarea>
              </div> 
                <div class="col-xs-12 col-sm-12 col-md-12">
                  
                  <a href="{{ route('noticia.allComentario') }}" class="btn btn-info btn-block" > Atrás</a></div>
          </div>

      </div>

<br>

   
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
<div class="w3-container">
</div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>

@endsection
