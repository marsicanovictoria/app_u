

@extends('layouts.app')

@section('content')

<!-- Navbar on small screens -->

<!-- Page Container -->
<div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:8%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
         <h4 class="w3-center">Publicaciones Recientes</h4>
        
         <hr>
         <p><i class="fa fa-pencil fa-fw w3-margin-right w3-text-theme"></i></p>
         <p><i class="fa fa-home fa-fw w3-margin-right w3-text-theme"></i> </p>
         <p><i class="fa fa-birthday-cake fa-fw w3-margin-right w3-text-theme"></i></p>
        </div>
      </div>
      <br>
      
     
      <!-- Interests --> 
      <div class="w3-card w3-round w3-white w3-hide-small">
        <div class="w3-container">
          
        </div>
      </div>
      <br>
      
      <!-- Alert Box -->
      <div class="w3-container w3-display-container w3-round w3-theme-l4 w3-border w3-theme-border w3-margin-bottom w3-hide-small">
        <span onclick="this.parentElement.style.display='none'" class="w3-button w3-theme-l3 w3-display-topright">
          <i class="fa fa-remove"></i>
        </span>
        <p><strong>Alertas</strong></p>
        <p></p>
      </div>
    
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div class="w3-container w3-padding">
              <h6 class="w3-opacity">Presentación </h6>
            
  
<table class="w3-table-all">
<tr>
  <th>Pensamiento</th>
  <th>Descripción</th>
  <th>Propósito</th>
  <th>2do Propósito</th>
  <th>3er Propósito</th>
  <th></th>
</tr>
@if($presentacion->count())  
@foreach($presentacion as $presentacion)
<tr>
<td>{{ $presentacion-> pensamiento}}</td>
<td>{{ $presentacion-> presentacion}}</td>
<td>{{ $presentacion-> proposito}}</td>
<td>{{ $presentacion-> proposito2}}</td>
<td>{{ $presentacion-> proposito3}}</td>
<td>{{ $presentacion-> activo}}</td>
<td>
        <a class="btn btn-primary btn-xs" class="btn btn-success btn-xs" href="{{action('PresentacionController@edit', $presentacion->id)}}" type="submit"><span class="glyphicon glyphicon-pencil">
        </span>
      </a>
    </td>

</tr>
@endforeach 
               @else
               <tr>
                <td colspan="8">No hay registro !!</td>
              </tr>
              @endif
</table>

            </div>
          </div>

        </div>
        
      </div>
<br>
            <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div class="w3-container w3-padding">
              <h6 class="w3-opacity">Comentarios </h6>
<table class="w3-table-all">
<tr>
  <th>Nombre</th>
  <th>Correo electrónico</th>
  <th>Comentarios</th>
  <th>Fecha</th>
</tr>
@if($presComentario->count())  
@foreach($presComentario as $presComentarios)
<tr>
<td>{{ $presComentarios-> nombre}}</td>
<td>{{ $presComentarios-> email}}</td>
<td>{{ $presComentarios-> comentarios}}</td>
<td>{{ $presComentarios-> created_at}}</td>
<td>
    <form action="{{action('PresentacionController@destroy', $presComentarios->id)}}" method="POST">
      {{csrf_field()}}
        <input name="_method" type="hidden" value="DELETE">
        <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash">
        </span>
      </button>
    </td>

</tr>
@endforeach 
               @else
               <tr>
                <td colspan="8">No hay registro !!</td>
              </tr>
              @endif
</table>
{{ $presComentario->links() }}

            </div>
          </div>
        </div>
      </div>
      
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
        <div class="w3-container">
          <button type="button" class="w3-button w3-block w3-theme-l4">
            <a href="{{ url('/administracion')}}" class="w3-button-up" title="Messages">Usuarios</a></button>
          <button type="button" class="w3-button w3-block w3-theme-l4"><a href="{{ url('/presentacion')}}" class="w3-button-up" title="Messages">Presentación</a></button>
          <button type="button" class="w3-button w3-block w3-theme-l4" data-toggle="modal" data-target="#myModal">Comentarios</button>
          <button type="button" class="w3-button w3-block w3-theme-l4" data-toggle="modal" data-target="#myModal">Blog</button>
        </div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
          


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!-- End Page Container -->
</div>
<br>
 
</html> 
