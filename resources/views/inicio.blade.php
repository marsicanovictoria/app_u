<!DOCTYPE html>
<html>
<title>Universitarios Unios</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="{{mix('css/app.css')}}" rel="stylesheet" type="text/css">

<body class="w3-theme-l5">


<!-- Links (sit on top) -->
<div class="w3-top">
 <div class="w3-bar w3-theme-d2 w3-left-align w3-large">
  <a href="#" class="w3-bar-item w3-padding-large"><img src="/img/logo_app.png" style="width:50%" class="w3-margin-bottom"></a>
 </div>
 <div class="w3-bar-up w3-theme-d2-up w3-left-align-up w3-large-up">
  <a href="{{ route('login') }}" class="w3-button-up2" title="Messages">{{ __('ENTRAR') }}</a>
  <a href="{{ route('register') }}" class="w3-button-up2" title="Messages">{{ __('REGISTRAR') }}</a>
    <a href="#presentacion" class="w3-button-up" title="Comunicaciones">PRESENTACIÓN</a>
    <a href="#formacion_personal" class="w3-button-up" title="Account Settings">FORMACIÓN PERSONAL</a>
    <a href="#blog" class="w3-button-up" title="Messages">BLOG OFICIAL</a>
    <a href="#consultas" class="w3-button-up" title="Messages">CONSULTAS</a>

  <a href="#arte" class="w3-button-up" title="Messages">ARTE</a>

</div>

 <div class="w3-bar-up w3-theme-d3-up w3-left-align-up w3-large-up">
<a href="#comunicaciones" class="w3-button-up" title="Comunicaciones">COMUNICACIONES</a>
  <a href="#noticias" class="w3-button-up" title="Account Settings">NOTICIAS</a>
  <a href="#investigaciones" class="w3-button-up" title="Messages">INVESTIGACIONES</a>
  <a href="#estudios" class="w3-button-up" title="Messages">ESTUDIOS</a>
  <a href="#tesis" class="w3-button-up" title="Messages">TESIS</a>
  <a href="#cursos" class="w3-button-up" title="Messages">CURSOS</a>
  <a href="#encuestas" class="w3-button-up" title="Messages">ENCUESTAS Y TENDENCIAS SOCIALES</a>

</div>

</div>


<!-- Add a background color and large text to the whole page -->
<div class="w3-sand w3-grayscale w3-large" style="margin-top:3%">

<!-- About Container -->
<div class="w3-container" id="about">
    <div class="contenedor" style="position: relative;
    display: inline-block;
    text-align: center;">
    <h5 class="w3-center w3-padding-64">
  <img class="mySlides" src="/img/s10.jpg" style="width:100%">
  <img class="mySlides" src="/img/s11.jpg" style="width:100%">
  <img class="mySlides" src="/img/s55.jpg" style="width:100%">
  <img class="mySlides" src="/img/s6.jpg" style="width:100%">
  <img class="mySlides" src="/img/s71.jpg" style="width:100%">
  <img class="mySlides" src="/img/s8.jpg" style="width:100%">
  <img class="mySlides" src="/img/s91.jpg" style="width:100%">
  <div class="w3-padding-65" id="div1"><h1>BIENVENIDO A UNIVERSITARIOS UNÍOS</h1></div>
    <div class="w3-padding-66" id="div1"><h4>La red social de todos los universitarios del Perú y del mundo <a href="{{ route('register') }}" class="w3-button-up">{{ __('Registrate Aquí') }} </a></h4></div>
</h5>
</div>
</div>

<!-- Menu Container -->
<div class="w3-container" id="presentacion">
  <div class="w3-content" style="max-width:700px">
 
    <h5 class="w3-center w3-padding-48"><span class="w3-tag w3-wide">PRESENTACIÓN</span></h5>
  
    <div class="w3-row w3-center w3-card w3-padding">
      <a href="javascript:void(0)" onclick="openMenu(event, 'Eat');" id="myLink">
        <div class="w3-col s6 tablink">Propósitos</div>
      </a>
      <a href="javascript:void(0)" onclick="openMenu(event, 'Drinks');">
        <div class="w3-col s6 tablink">Estructura</div>
      </a>
    </div>

    <div id="Eat" class="w3-container menu w3-padding-48 w3-card">
      
      <p>1. Fomentar la comunicación entre estudiantes y profesionales universitarios; así como entre los primeros con los que se encuentran ejerciendo su profesión en el mundo laboral, en ambos casos, con propósitos de intercambio de información y experiencias; y de crecimiento y superación personal, para una acción mas eficaz, eficiente y efectiva en sus respectivos contextos.</p><br>

      <p>2. Desarrollar, implementar y ofrecer el Programa de Formnación Pesonal, via virtual, con la participación de lo más distiguidos profesionales de todos los campos de actividad de los universitarios y en coodinación con las universidades.</p><br>

      <p>3. Promover e impulsar desde dentro y fuera de la universidad, mediante la acción coordinada de los universitarios, una verdadera reforma universitaria, a partir de un nuevo diseño curricular y un régimen académico apropiado, orientados hacia la formación integral del estudiante universitario, y la participación de la universidad en su conjunto, en el desarrollo sostenible de las comunidades de sus áreas de influencia, en el contexto local, regional y mundial.</p><br>

      <p>4. Promover la partición de las universidades y de los universitarios en el desarrollo sostenible de las comunidades locales de sus contextos de influencia y de los contextos regionales, nacionales y mundial, en el mejoramiento de las condiciones de calidad de vida mediante el acceso de las mayorías a los beneficios de la ciencia y la tecnología y demás bienes culturales logrados por la humanidad, sobre la base de su acceso a la educación y el fomento de la paz y la seguridad personal y social en cada contexto.</p><br>
    </div>

    <div id="Drinks" class="w3-container menu w3-padding-48 w3-card">
      
      <p>1. Presentación y/o Editorial</p><br>

      <p>2. Noticias y Eventos</p><br>

      <p>3. El Programa de Formación Personal</p><br>

      <p>4. Investigación</p><br>

      <p>5. Comunicaciones</p><br>

      <p>6. Blog del DR. EHF</p><br>

      <p>7. Consultas</p><br>

      <p>8. Estudios y Propuestas</p><br>

      <p>9. Tesis y Libros</p><br>

      <p>10. Cursos y Programas de Capacitación</p><br>

      <p>11. Artes, Cultura y Amenidades</p><br>

      <p>12. Otros</p>
    </div>  
  </div>
</div>

<div class="w3-container" id="formacion_personal" style="padding-bottom:32px;">
  <div class="w3-content" style="max-width:700px">
    <h5 class="w3-center w3-padding-48"><span class="w3-tag w3-wide">PROGRAMA DE FORMACIÓN PERSONAL</span></h5>
    <p>Tiene como objetivo principal poner a la persona como centro de la acción de cada universitario , para auto-observarse, descrubir sus fortalezas y carencias, para incrementarlas en un caso y desarrollarlas o robustecerlas en otros, con miras a un crecimiento y desarrollo personal, para ser más eficaz y competente en su desempeño profesional y en su rol social orientado a generar desde su profesión las condiciones para eñ mejoramiento de la calidad de vida de las comunicaciones donde actúa, como un deber moral del más alto nivel de responsabilidad social. Esto implica necesariamente< un cambio de mentalidad de todos los universitarios, a partir de la asunción de un nuevo compromiso con el cambio y el desarrollo de los entornos donde actúa.</p>
    <p>Los contenidos del Programa de Formación serán desarrollados por equipos polivalentes de amplia experiencia en el ejercicio de su profesión en el mundo laboral y/o académico, cuyo interés y compromiso mayor sea la Formación Personal de los universitarios.
    </p>

  </div>
</div>

<div class="w3-container" id="blog" style="padding-bottom:32px;">
  <div class="w3-content" style="max-width:700px">
    <h5 class="w3-center w3-padding-48"><span class="w3-tag w3-wide">BLOG DEL DR. HORNA Y SUS INVITADOS</span></h5>
    <p>Está orientado a ofrecer el pensamiento del creador y promotor  de esta red social, tratará de mantener la orientación para el cumplimiento de sus propósitos y será el medio a través del cual exponga sus conceptos y opiniones sobre los temas universitarios, educativos y sociales que son objetos de sus estudios e investigaciones.</p>

    <p>Propiciará la pluralidad de enfoques sobre un mismo tema, que enriquezcan el pensamiento y accionar de los universitarios, con miras a contribuir al mejoramiento cualitativo de la realidad universitaria y social del país en el contexto mundial. Se espera y aspira a un diálogo fecundo.</p>
    
  </div>
</div>

<!-- Contact/Area Container -->
<div class="w3-container" id="consultas" style="padding-bottom:32px;">
  <div class="w3-content" style="max-width:700px">
    <h5 class="w3-center w3-padding-48"><span class="w3-tag w3-wide">CONSULTAS</span></h5>
    <p>Este portal te ofrece la oportunidad de brindarte la atención de expertos para ayudarte en el ánálisis de las situaciones conflictivasque puedes estar afrontando a nivel personal o de tus interrelaciones, que pudieran estar robándote la energía que requieres para buscar y encontrar las soluciones apropiadas a otros problemas, quizás más complejos, sentirte fortalecido y, vivir de un modo integral y con optimismo en el aquí y ahora, que es donde puedes construir tu futuro.</p>
    
  </div>
</div>
<div class="w3-container" id="arte" style="padding-bottom:32px;">
  <div class="w3-content" style="max-width:700px">
    <h5 class="w3-center w3-padding-48"><span class="w3-tag w3-wide">ARTE</span></h5>
    <p>Este será un portal orientado a nutrir el cuerpo, la mente y/o espíritu con las expresiones sublimes del arte y la cultura, donde los universitarios podrán colgar y difundir su producción artístico-cultural, con el propósito de captar seguidores y motivar a otros universitarios a despertar su interés por alguna de las formas de expresión artístico-cultural. Por lo pronto se han considerado las siguientes: Música, Danza, Pintura, Pensamientos, Dichos, Refranes; Amenidades y Chistes.</p>
    
  </div>
</div>
<div class="w3-container" id="comunicaciones" style="padding-bottom:32px;">
    <div class="w3-content" style="max-width:700px">
    <h5 class="w3-center w3-padding-48"><span class="w3-tag w3-wide">COMUNICACIONES</span></h5>
    <p>Este portal debe convertirse en el más dinámico de la red, porque cumple uno de los principales propósitos con que se ha creado esta red: fomentar y mantener una fluida comunicación entre universitarios. Todos los universitarios registrados en la red tendrán la oportunidad de ver tu comunicación; muchos se interesarán en contestarla. Así irás conociendo personas que tienen interés en los temas  o asuntos que a ti te interesan, apuntando a una mejora continua de los distintos componentes y procesos que se dan en una univeridad, así como de ella misma. Por ello tus comunicaciones deben reflejar lo que piensas y/o sientes respecto a lo que acontece en la universidad o lo que desearías que ocurriera a fin de tener las mejores oportunidades para disfrutar de una calidad pertinente, que esperas, tu universidad te brinde.</p>    
  </div>
</div>

<div class="w3-container" id="noticias" style="padding-bottom:32px;">
    <div class="w3-content" style="max-width:700px">
    <h5 class="w3-center w3-padding-48"><span class="w3-tag w3-wide">NOTICIAS</span></h5>
    <p>Es la "Gaceta" de los universitarios. Publica en este portal los acontecimientos que pueden considerarse como una noticia útil para los demás universitarios, quizás se trate de algo que ocurrió en tu universidad y consideres un buen ejemplo a seguir por los estudiantes de otras escuelas o universidades; el propósito es difundir los acontecimientos que nos enriquezcan personal o colectivamente, que traen y atraen el bienestar para todos.</p>    
    <p>Publica también los eventos que vienes preparando, que quisieras que los demás universitarios conozcan o asistan. Hazlo con la debida anticipación. Lo que se hace a última hora no siempre sale bien.</p>
  </div>
</div>

<div class="w3-container" id="investigaciones" style="padding-bottom:32px;">
    <div class="w3-content" style="max-width:700px">
    <h5 class="w3-center w3-padding-48"><span class="w3-tag w3-wide">INVESTIGACIONES</span></h5>
    <p>La Red Social Universitarios Uníos te brinda la oportunidad de utilizar internet  para realizar tus investigaciones, la formulación de tu Proyecto de Investigación, la implementación técnica del mismo, así como la ejecución y elaboración del informe respectivo, proporcionándote las herramientas que necesites utilizar e incluso sirviendo de Hosting para que cuelgues los instrumentos de Recojo de la Informacion, los procese automáticamente y obtengas los resultados que integrarás a tu informe, con lo cual estaremos haciendo un importante aporte para que los estudiantes universitarios logren sus metas de formación profesional y las universidades puedan difundir y usar las herramientas propias de la investigación científica y tecnológica y de este modo cumplir con una de las más importantes funciones de la Universidad en la era de la Sociedad del Conocimiento, y además logren sus metas para la Acreditación. Así nuestra Red Social se convertirá en el medio que nos permitirá a todos alcanzar nuestras metas en el campo de la investigación a nivel personal, institucional y de la sociendad en general, que requiere y espera el aporte de la universidad para lograr un desarrollo sostenible.</p>
  </div>
</div>

<div class="w3-container" id="estudios" style="padding-bottom:32px;">
    <div class="w3-content" style="max-width:700px">
    <h5 class="w3-center w3-padding-48"><span class="w3-tag w3-wide">ESTUDIOS</span></h5>
    <p>Este protal te permitirá publicar y difundir entre los universitarios de todo el mundo tus estudios y propuestas frente a los problemas de la realidad que son objeto de tu interés conocerlos para plantear soluciones eficaces y comprometerte con las mismas. Tendrás  la oportunidad de recibir un feedback que te permita valorar tus aportes y consecuentemente mejorarlos de ser necesarios, perderás el temor que caracteriza a muchos universitarios de dar a conocer sus hallazgos y someterlos al escrutinio público. Esto contribuirá a enriquecerte y a sentirte digno de ser escuchado. Al registrar tus aportes actúa con la mayor honestidady responsabilidad, a fin de que lo que ingreses, corresponda realmente a tu autoría y honre tu condición de universitario.
      
    </p>
  </div>
</div>

<div class="w3-container" id="tesis" style="padding-bottom:32px;">
    <div class="w3-content" style="max-width:700px">
    <h5 class="w3-center w3-padding-48"><span class="w3-tag w3-wide">TESIS</span></h5>
    <p>Este portal presenta inicialmente las DOS (2) Tesis Doctorales del Doctor Ernesto Horna Figueroa, en su versión, La primera, titulada: LA FORMACIÓN PERSONAL EN EL CURRÍCULO UNIVERSITARIO FRENTE AL CONTEXTO GLOBALIZADO (2013); la segunda: RELACIÓN ENTRE LA INTELIGENCIA EMOCIONAL, LA TOMA DE DECISIONES Y ÉXITO EN EL LOGRO DE METAS EN LOS ESTUDIOS Y EL TRABAJO (2005).
    </p>
    <p>El acceso y lectura de la tesis, aparte de dar a conocer sus hallazgos, permitirá financiar, en parte, el costo de la implementación técnica del Programa de Formación Personal que es, como se ha señalado, el principal motivo que anima la creación de la Red Social Universitarios Uníos.</p>
    <p>Por ello el acceso y lectura de cada una de las tesis tendrá un costo simbólico de diez soles en el Perú y de diez dólares en el extranjero, que no limita la intención de un aporte mayor.</p>
    <p>Esperamos tu comprensión y respeto a la propiedad intelectual, evitando o no intentando bajar o copiar la tesis elegida, a la cual tendrás acceso permanete con el código personal que se te asigne al hacer el depósito bancario por el monto indicado. Este será tu mejor aporte y colaboración que agradecemos anticipadamente porque viabilizará el cumnplimiento de nuestro propósito más importante.</p>
    <p>En el portal correspondiente encontrarás los números de las cuentas en soles y en dólares y se indica el procedimiento a seguir para informar sobre el depósito realizado y recibir el código personal para ingresar a la lectura de la tesis.</p>
  </div>
</div>

<div class="w3-container" id="cursos" style="padding-bottom:32px;">
    <div class="w3-content" style="max-width:700px">
    <h5 class="w3-center w3-padding-48"><span class="w3-tag w3-wide">CURSOS</span></h5>
    <p>Con el propósito de contribuir a que las universidades asuman el cambio que la sociedad espera respecto a la Formación Integral de sus futuros profesionales, con vocación y compromiso por el estudio del contexto, para la generación de condiciones que impulsen, el mejoramiento de la calidad de vida en las comunidades donde actúen y promover un desarrollo sostenible, ofrecemos de modo permanente, utilizando, preferentemente, la metodología de la Educación a Distanciam, los tres cursos siguientes:</p>
    <p>Capacitación en el Desarrollo y Conducción de Programas de Formación Personal en las Universidades. (200 horas). Dirigido a los docentes universitarios prioritariamente.</p>
    <p>Capacitación en el manejo de las TICs Aplicada al Proceso de Aprendizaje Enseñanza y Autoaprendizaje. (100 horas) Dirigido preferentemente a los docentes universitarios.</p>
    <p>Capacitación en la elaboración de Materiales Educativos Virtuales (100 horas). Dirigido a los docentes universitarios prioritariamente.</p>
  </div>
</div>

<!-- End page content -->
</div>



<script>
// Tabbed Menu
function openMenu(evt, menuName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("menu");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(menuName).style.display = "block";
  evt.currentTarget.firstElementChild.className += " w3-dark-grey";
}
document.getElementById("myLink").click();


var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 5000); // Change image every 2 seconds
}

</script>

</body>
</html>
