

@extends('layouts.app')

@section('content')

<!-- Navbar on small screens -->

<!-- Page Container -->
<div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:8%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
         <h4 class="w3-center">Publicaciones Recientes</h4>
        
         <hr>
         <p><i class="fa fa-pencil fa-fw w3-margin-right w3-text-theme"></i></p>
         <p><i class="fa fa-home fa-fw w3-margin-right w3-text-theme"></i> </p>
         <p><i class="fa fa-birthday-cake fa-fw w3-margin-right w3-text-theme"></i></p>
        </div>
      </div>
      <br>
      
     
      <!-- Interests --> 
      <div class="w3-card w3-round w3-white w3-hide-small">
        <div class="w3-container">
          
        </div>
      </div>
      <br>
      
      <!-- Alert Box -->
    
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div class="w3-container w3-padding">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Error!</strong> Revise los campos obligatorios.<br><br>
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if(Session::has('success'))
      <div class="alert alert-info">
        {{Session::get('success')}}
      </div>
      @endif

      <form method="POST" action="{{ route('blog.update',$blog->id) }}"  role="form" enctype="multipart/form-data">
              {{ csrf_field() }}
              <p>Título</p>
              <input name="_method" type="hidden" value="PATCH">
               <div class="form-group">
                <textarea name="titulo" class="form-control input-sm"  placeholder="Titulo">{{$blog->titulo}}</textarea>
              </div>
              <p>Contenido</p>
              <div class="form-group">
                <textarea rows="15" cols="50" maxlength="2000" name="contenido" class="form-control input-sm"  placeholder="Contenido">{{$blog->contenido}}</textarea>
              </div>
          <div class="plan-name-bronze">
            
          </div>
            </br>


              </br>
              <div class="row">
 
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <input type="submit"  value="Actualizar" class="w3-button w3-theme-d2 w3-margin-bottom">
                </div>  
 
              </div>
            </form>



        </div>
          </div>
        </div>
        
      </div>
<br>
            <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div class="w3-container w3-padding">
              <p>Comentarios</p>
<table class="w3-table-all">
<tr>
  <th>Nombre</th>
  <th>Email</th>
  <th>Comentario</th>
  <th>Fecha</th>
</tr>

@foreach($blogcomen as $blogcomens)
<tr>
<td>{{ $blogcomens-> nombre}}</td>
<td>{{ $blogcomens-> email}}</td>
<td>{{ $blogcomens-> comentario}}</td>
<td>{{ $blogcomens-> created_at}}</td>
    <td>
     <form action="{{action('BlogIndexController@destroy', $blogcomens->id)}}" method="POST">
      {{csrf_field()}}
        <input name="_method" type="hidden" value="DELETE">
        <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash">
        </span>
      </button>
    </td>
</tr>
@endforeach 

</table>



        </div>
        {{ $blogcomen->links() }}
          </div>
        </div>
        
      </div>
      
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
        <div class="w3-container">
          <button type="button" class="w3-button w3-block w3-theme-l4">
            <a href="{{ url('/administracion')}}" class="w3-button-up" title="Messages">Usuarios</a></button>
          <button type="button" class="w3-button w3-block w3-theme-l4"><a href="{{ url('/presentacion')}}" class="w3-button-up" title="Messages">Presentación</a></button>
          <button type="button" class="w3-button w3-block w3-theme-l4" data-toggle="modal" data-target="#myModal">Comentarios</button>
          <button type="button" class="w3-button w3-block w3-theme-l4">
            <a href="{{ url('/blog')}}" class="w3-button-up" title="Messages">Blog</a></button>
        </div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
          


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!-- End Page Container -->
</div>
<br>
 
</html> 
