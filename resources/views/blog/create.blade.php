

@extends('layouts.app')

@section('content')

<!-- Navbar on small screens -->

<!-- Page Container -->
<div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:8%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->

      <br>
      
     
      <!-- Interests --> 
      <div class="w3-card w3-round w3-white w3-hide-small">
        <div class="w3-container">
          
        </div>
      </div>
      <br>
      
      <!-- Alert Box -->
    
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div class="w3-container w3-padding">

      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Error!</strong> Revise los campos obligatorios.<br><br>
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if(Session::has('success'))
      <div class="alert alert-info">
        {{Session::get('success')}}
      </div>
      @endif
 
        
          <div class="table-container">
            <form method="POST" action="{{ route('blog.store') }}"  role="form" enctype="multipart/form-data">
              {{ csrf_field() }}
 
              <div class="form-group">
                <textarea name="titulo" id="titulo" class="form-control input-sm" placeholder="Titulo"></textarea>
              </div>
              <div class="form-group">
                    <textarea rows="15" cols="50" maxlength="5000" name="contenido" id="contenido" class="form-control input-sm" placeholder="Contenido"></textarea>
              </div>
              <div class="form-group">
                <img width="50px" src="/img/subir_imagen.png">
                <input type="file" name="blogImagen" >
              </div>
              <div class="form-group">
               <img width="50px" src="/img/pdf.jpg">
                <input type="file" name="archivopdf" class="form-control input-sm"  >
              </div>
              <div class="form-group">
                <img width="50px" src="/img/ppt.jpg">
                <input type="file" name="Archivoppt" class="form-control input-sm"  >
              </div>
              <div class="form-group">
                 <img width="50px" src="/img/word.png">
                <input type="file" name="archivodocx" class="form-control input-sm"  >
              </div>
              <div class="form-group">
                <img width="50px" src="/img/excel.png">
                <input type="file" name="archivoxlsx" class="form-control input-sm"  >
              </div>
              <div class="form-group">
                <img width="50px" src="/img/video.jpg">
                <input type="file" name="archivovideo" class="form-control input-sm"  >
              </div> 
<div class="col-xs-12 col-sm-12 col-md-12">
                  <input type="submit"  value="Subir" class="btn btn-success btn-block">
                  <a href="{{ url('/administracion') }}" class="btn btn-info btn-block" > Atrás</a></div>
            </form>

 

          </div>
        </div>
          </div>
        </div>
        
      </div>
      
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
        <div class="w3-container">
          <button type="button" class="w3-button w3-block w3-theme-l4">
            <a href="{{ url('/administracion')}}" class="w3-button-up" title="Messages">Usuarios</a></button>
          <button type="button" class="w3-button w3-block w3-theme-l4"><a href="{{ url('/presentacion')}}" class="w3-button-up" title="Messages">Presentación</a></button>
          <button type="button" class="w3-button w3-block w3-theme-l4" data-toggle="modal" data-target="#myModal">Comentarios</button>
          <button type="button" class="w3-button w3-block w3-theme-l4" data-toggle="modal" data-target="#myModal">Blog</button>
        </div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
          


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!-- End Page Container -->
</div>
<br>
 
</html> 
