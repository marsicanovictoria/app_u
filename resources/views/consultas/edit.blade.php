@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
            <div class="w3-card w3-round w3-white">
      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
<button type="button" class="w3-button w3-block w3-theme-l4">
            <a href="{{  route('consultas.administracion') }}" class="w3-button-up" title="Messages">Atrás</a></button>
                  </div>
                          </div>
      </div> 

      <br>   
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding .w3-padding-blog">

        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container">
            <h2>Consultas</h2>
<div class="table-container">
            <form method="POST" action="{{action('ConsultasController@update', $consultas->id)}}"  role="form" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="form-group">
                Nombre completo
                <input type="text" name="nombre" id="nombre" maxlength="25" class="form-control input-sm" value="{{ $consultas->  nombre }}" disabled="true">
              </div>
              <div class="form-group">
                Pseudónimo
                <input type="text"  name="pseudonimo" id="pseudonimo" class="form-control input-sm" maxlength="25" value="{{ $consultas->  pseudonimo }}" disabled="true">
              </div>
              <div class="form-group">
                Correo electrónico
                <input type="text" name="email" id="email" class="form-control input-sm" maxlength="50" value="{{ $consultas->  email }}" disabled="true">
              </div>
                            <div class="form-group">
                Teléfono
                <input type="text" name="telefono" id="telefono" class="form-control input-sm" maxlength="50" value="{{ $consultas->  telefono }}" disabled="true">
              </div>
                            <div class="form-group">
                              Consulta
                <textarea name="consulta" id="consulta" 
                maxlength="150"class="form-control input-sm" value="{{ $consultas->  consulta }}" disabled="true"></textarea>
              </div>
              <div class="form-group">
                              Respuesta
                <textarea name="respuesta" id="respuesta" 
                maxlength="3780" rows="30" class="form-control input-sm" placeholder="Respuesta"></textarea>
              </div>
 <input type="hidden" id="id_usu_res" name="id_usu_res" value="{{ auth()->id() }}">
          <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
             <div class="captcha">
               <span>{!! captcha_img('flat') !!}</span>

               </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
             <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha"></div>
          </div>
 
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <input type="submit"  value="Guardar" class="btn btn-success btn-block">
                  </div>
                             
            </form>
          </div>
            </div>
          </div>
             <br>
        </div>

      </div>

<br>

   
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
<div class="w3-container">
</div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>
<script type="text/javascript">
  var texto, padre;
$(".contenidoBlog").each(function(){
    texto = $(this).html();
    this.setAttribute("data-texto", texto);
    if ($(this).html().length > 75){
        $(this)
            .html(texto.substr(0, 75) + "...");
    }
});
 

</script>
@endsection
