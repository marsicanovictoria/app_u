@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->

      <br>   
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container w3-padding" >
    <div class="pricing-table pricing-three-column row">
          <div class="w3-container"  style="padding-bottom:32px;">
 <h2>{{ $noticias->  created_at->toFormattedDateString()}}</h2>
          <div class="plan-name-bronze">

            <h2><i>{{ $noticias->  titulo}}</i></h2>
            <br>
            <p>{{ $noticias->  contenido}}</p>
            
          </div>
          
@if($noticias-> imagen != NULL)
          <div class="plan-name-bronze">
            <img width="70%" src="{{ Storage::url($noticias->imagen) }}">
          </div>
          @endif
@if($noticias-> pdf != NULL)
        <a href="{{ action('NoticiasController@pdf', $noticias->id) }}">
            <img width="20px" src="/img/pdf.jpg">
        </a>
        @endif
@if($noticias-> ppt != NULL)
        <a href="{{ action('NoticiasController@ppt', $noticias->id) }}" >
            <img width="20px" src="/img/ppt.jpg">
        </a>
        @endif
@if($noticias-> word != NULL)
        <a href="{{ action('NoticiasController@word', $noticias->id) }}" >
            <img width="20px" src="/img/word.png">
        </a>
        @endif
@if($noticias-> excel != NULL)
        <a href="{{ action('NoticiasController@excel', $noticias->id) }}" >
            <img width="20px" src="/img/excel.png">
        </a>
@endif
@if($noticias-> video != NULL)
        <a href="{{ action('NoticiasController@video', $noticias->id) }}" >
            <img width="20px" src="/img/video.jpg">
        </a>
@endif
        </div>
    </div>
            </div>
             <a href="{{ route('noticia.index') }}" class="w3-button-up" title="Messages"><span class="glyphicon glyphicon-backward">
        </span>Atrás</a>
          </div>
             <br>
        </div>


      </div>
  
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
<div class="w3-container">
</div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>

@endsection
