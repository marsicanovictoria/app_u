@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
      <div class="w3-card w3-round w3-white">


      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
          
        </div>
      </div>    

      </div>
      <br>   
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding .w3-padding-blog">

<ul class="collection with-header">
    <li class="collection-header">
        <h3 class="flow-text">Encuestas Recientes
            <span style="float:right;">{{ link_to_route('new.survey', 'Crear') }}
            </span>
        </h2>
    </li>
        {{-- expr --}}
    @foreach ($surveys as $survey)
      <li class="collection-item">
        <div>
            {{ link_to_route('detail.survey', $survey->title, ['id'=>$survey->id])}}
            <a href="survey/view/{{ $survey->id }}" title="Take Survey" class="secondary-content"><i class="material-icons">send</i></a>
            <a href="survey/{{ $survey->id }}" title="Edit Survey" class="secondary-content"><i class="material-icons">edit</i></a>
            <a href="survey/answers/{{ $survey->id }}" title="View Survey Answers" class="secondary-content"><i class="material-icons">textsms</i></a>
        </div>
        </li>
    @endforeach
    
    </ul>

      </div>
 <a href="" class="w3-button-up" title="Messages"><span class="glyphicon glyphicon-backward">
        </span>Atrás</a>
<br>

   
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
        <div class="w3-container w3-padding">
          <button type="button" class="w3-button w3-block w3-theme-l4">
            <a href="" class="w3-button-up" title="Messages">Nueva +</a></button>
          <button type="button" class="w3-button w3-block w3-theme-l4"><a href="" class="w3-button-up" title="Messages">Mis Aportes</a></button>

        </div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>

@endsection
