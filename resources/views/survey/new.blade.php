@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
      <div class="w3-card w3-round w3-white">


      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
          
        </div>
      </div>    

      </div>
      <br>   
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding .w3-padding-blog">

<div class="card-content">
      <span class="card-title">Agregar Encuesta</span>
      <form method="POST" action="create" id="boolean">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">    
          <div class="input-field col s12">
            <input name="title" id="title" type="text">
            <label for="title">Título</label>
          </div>          
          <div class="input-field col s12">
            <textarea name="description" id="description" class="materialize-textarea"></textarea>
            <label for="description">Descripción</label>
          </div>
        </div>
                <div class="row">    
          <div class="input-field col s12">
            <input name="title" id="pregunta1" type="text">
            <label for="title">Pregunta</label>
          </div>          
          <div class="input-field col s12">
            <input name="title" id="pregunta2" type="text">
            <label for="title">Pregunta</label>
          </div> 
                    <div class="input-field col s12">
            <input name="title" id="pregunta3" type="text">
            <label for="title">Pregunta</label>
          </div> 
                    <div class="input-field col s12">
            <input name="title" id="pregunta4" type="text">
            <label for="title">Pregunta</label>
          </div> 
                    <div class="input-field col s12">
            <input name="title" id="pregunta5" type="text">
            <label for="title">Pregunta</label>
          </div>          
          <div class="input-field col s12">
          <button class="btn waves-effect waves-light">Enviar</button>
          </div> 
        </div>
        </form>
    </div>

      </div>
 <a href="" class="w3-button-up" title="Messages"><span class="glyphicon glyphicon-backward">
        </span>Atrás</a>
<br>

   
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
        <div class="w3-container w3-padding">
          <button type="button" class="w3-button w3-block w3-theme-l4">
            <a href="" class="w3-button-up" title="Messages">Nueva +</a></button>
          <button type="button" class="w3-button w3-block w3-theme-l4"><a href="" class="w3-button-up" title="Messages">Mis Aportes</a></button>

        </div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>

@endsection
