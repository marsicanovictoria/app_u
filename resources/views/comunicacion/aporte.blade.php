
@extends('layouts.app')

@section('content')

<!-- Navbar on small screens -->

<!-- Page Container -->
<div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:8%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
      <div class="w3-card w3-round w3-white">

      </div>
      <br>
      
     

      <br>

    
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div class="w3-container w3-padding">
              <h6 class="w3-opacity">Comunicacion </h6>
            </div>
              <br>             
  
<table class="w3-table-all">
<tr>
  <th>Titulo</th>
  <th>Eje</th>
  <th>Fecha</th>
  <th></th>
  <th></th>
</tr>
@if($comunicacion->count())  
@foreach($comunicacion as $comunicacions)
<tr>
<td>{{ $comunicacions-> titulo }}</td>
<td> {{ $comunicacions-> eje }}</td>
<td>{{ $comunicacions-> created_at }}</td>
<td>
        <a class="btn btn-primary btn-xs" class="btn btn-success btn-xs" href="{{action('ComunicacionController@edit', $comunicacions->id)}}" type="submit"><span class="glyphicon glyphicon-pencil">
        </span>
      </a>
    </td>
    <td>
     <form action="{{action('ComunicacionController@destroy', $comunicacions->id)}}" method="POST">
      {{csrf_field()}}
        <input name="_method" type="hidden" value="DELETE">
        <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash">
        </span>
      </button>
    </td>
</tr>
@endforeach 
               @else
               <tr>
                <td colspan="8">No hay registro !!</td>
              </tr>
              @endif
</table>

          </div>
          {{ $comunicacion->links() }}
        </div>
        
      </div>
      
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
        <div class="w3-container w3-padding">
          <button type="button" class="w3-button w3-block w3-theme-l4">
            <a href="{{ route('comunicacion.create')}}" class="w3-button-up" title="Messages">Nueva comunicación</a></button>
          
        </div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  <!-- Modal -->

<!-- End Page Container -->
</div>
<br>


</script>
 
</html> 
