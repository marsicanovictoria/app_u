@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->

      <br>   
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container w3-padding" >
    <div class="pricing-table pricing-three-column row">
          <div class="w3-container"  style="padding-bottom:32px;">
 <h2>{{ $arte->  created_at->toFormattedDateString()}}</h2>
          <div class="plan-name-bronze">

            <h2><i>{{ $arte->  titulo}}</i></h2>
            <br>
            <p>{{ $arte->  contenido}}</p>
            
          </div>
@if($arte-> imagen != NULL)
          <div class="plan-name-bronze">
            <img width="70%" src="{{ Storage::url($arte->imagen) }}">
          </div>
          @endif
@if($arte-> pdf != NULL)
        <a href="{{ action('ArteController@pdf', $arte->id) }}">
            <img width="20px" src="/img/pdf.jpg">
        </a>
        @endif
@if($arte-> ppt != NULL)
        <a href="{{ action('ArteController@ppt', $arte->id) }}" >
            <img width="20px" src="/img/ppt.jpg">
        </a>
        @endif
@if($arte-> word != NULL)
        <a href="{{ action('ArteController@word', $arte->id) }}" >
            <img width="20px" src="/img/word.png">
        </a>
        @endif
@if($arte-> excel != NULL)
        <a href="{{ action('ArteController@excel', $arte->id) }}" >
            <img width="20px" src="/img/excel.png">
        </a>
        @endif
@if($arte-> video != NULL)
        <a href="{{ action('ArteController@video', $arte->id) }}" >
            <img width="20px" src="/img/video.jpg">
        </a>
        
@endif
        </div>
    </div>
            </div>
             <a href="{{ route('arte.presentacion') }}" class="w3-button-up" title="Messages"><span class="glyphicon glyphicon-backward">
        </span>Atrás</a>
          </div>
             <br>
        </div>


      </div>

            <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container w3-padding">
  <div class="w3-content" style="max-width:700px">
    <p>Comentarios</p>
    
<form method="POST" action=""  role="form">
              {{ csrf_field() }}
      <p><input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <input class="w3-input w3-padding-16 w3-border" type="text"  name="comentarios" id="comentarios" required value=""></p>
        <p>Nombre</p>
        <p><input class="w3-input w3-padding-16 w3-border" type="text"  name="nombre" id="nombre" required value=""></p>
        <p>Correo electrónico</p>
        <p><input class="w3-input w3-padding-16 w3-border" type="text"  name="email" id="email" required value=""></p>
      <button class="w3-button w3-theme-d2 w3-margin-bottom" type="submit">Enviar</button></p>
    </form>

    </div>
            </div>
          </div>
        </div>

      </div>
  
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
<div class="w3-container">
</div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>

@endsection
