@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
      <div class="w3-card w3-round w3-white">


      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
          
        </div>
      </div>    

      </div>
      <br>   
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding .w3-padding-blog">
<h6 class="w3-opacity">Noticias y eventos</h6>
      <table class="w3-table-all">
<tr>
  <th>Titulo</th>
  <th>Carrera</th>
  <th>Entidad</th>
  <th>Fecha</th>
  <th></th>
  <th></th>
</tr>
@if($noticias->count())  
@foreach($noticias as $noticia)
<tr>
<td>{{ $noticia-> titulo }}</td>
<td> {{ $noticia-> carrera }}</td>
<td> {{ $noticia-> entidad }}</td>
<td>{{ $noticia-> created_at }}</td>
<td>
        <a class="btn btn-primary btn-xs" class="btn btn-success btn-xs" href="{{action('NoticiasController@edit', $noticia->id)}}" type="submit"><span class="glyphicon glyphicon-pencil">
        </span>
      </a>
    </td>
    <td>
     <form action="{{action('NoticiasController@destroy', $noticia->id)}}" method="POST">
      {{csrf_field()}}
        <input name="_method" type="hidden" value="DELETE">
        <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash">
        </span>
      </button>
    </td>
</tr>
@endforeach 
               @else
               <tr>
                <td colspan="8">No hay registro !!</td>
              </tr>
              @endif
</table>
{{ $noticias->links() }}
      </div>
 <a href="{{ route('noticia.all') }}" class="w3-button-up" title="Messages"><span class="glyphicon glyphicon-backward">
        </span>Atrás</a>
<br>

   
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
        <div class="w3-container w3-padding">
          <button type="button" class="w3-button w3-block w3-theme-l4">
            <a href="{{ route('noticia.create')}}" class="w3-button-up" title="Messages">Nueva +</a></button>


        </div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>

@endsection
