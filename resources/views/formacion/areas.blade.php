@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">

      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
                  <div class="w3-container w3-padding">
          <button type="button" class="w3-button w3-block w3-theme-l4"><a href="{{ url('/formacion') }}" class="w3-button-up" title="Messages">Atrás</a></button>
        </div>
        </div>
      </div> 
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding .w3-padding-blog">
        
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
          <div class="w3-container" style="padding-bottom:32px;">
                <div class="w3-third">
                  <a href="{{ action ('FormacionController@persona') }}"><img src="/img/LA-PERSONA.jpg" class="w3-round w3-margin-bottom" alt="Random Name" style="width:60%"></a>
                  
                </div>
                <div class="w3-third">
                  <a href="{{ action ('FormacionController@contexto') }}"><img src="/img/ELCONTEXTO.jpg" class="w3-round w3-margin-bottom" alt="Random Name" style="width:60%"></a>
                
                </div>
                <div class="w3-third">
                  <a href="{{ action ('FormacionController@conocimiento') }}"><img src="/img/SOCIEDADCONOCIMIENTO.jpg" class="w3-round w3-margin-bottom" alt="Random Name" style="width:60%"></a>
                  
                </div>
          </div>
          <div class="w3-container" style="padding-bottom:32px;">
                <div class="w3-third">
                  <a href="{{ action ('FormacionController@tecnologia') }}"><img src="/img/CIENCIAYTECNOLOGIA.jpg" class="w3-round w3-margin-bottom" alt="Random Name" style="width:60%"></a>
                  
                </div>
                <div class="w3-third">
                  <a href="{{ action ('FormacionController@empresa') }}"><img src="/img/LA-EMPRESA.jpg" class="w3-round w3-margin-bottom" alt="Random Name" style="width:60%"></a>
                
                </div>
          </div>
          </div>
             <br>
        </div>
      </div>
<br>
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">

      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>

@endsection
