

@extends('layouts.app')

@section('content')

<!-- Navbar on small screens -->

<!-- Page Container -->
<div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:8%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
      <div class="w3-card w3-round w3-white">

      </div>
      <br>
      
     

      
      <!-- Alert Box -->
    
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div class="w3-container w3-padding">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Error!</strong> Revise los campos obligatorios.<br><br>
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if(Session::has('success'))
      <div class="alert alert-info">
        {{Session::get('success')}}
      </div>
      @endif

      <form method="POST" action="{{ 
route('formacion.updatePresentacion', $formacion->  id)
       }}"  role="form" enctype="multipart/form-data">
              {{ csrf_field() }}
              <p>Contenido</p>
              <div class="form-group">
                <textarea rows="15" cols="50" maxlength="2000" name="contenido" class="form-control input-sm" rows="5"  placeholder="Contenido">{{ $formacion->  contenido}}</textarea>
              </div>

              </br>
              <div class="row">
 
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <input type="submit"  value="Actualizar" class="w3-button w3-theme-d2 w3-margin-bottom">
                </div>  
 
              </div>
            </form>



        </div>
          </div>
        </div>
        
      </div>
<br>

      
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">

    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
          


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!-- End Page Container -->
</div>
<br>
 
</html> 
