@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
      <div class="w3-card w3-round w3-white">


      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
          
        </div>
      </div>    

      </div>
      <br>   
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding .w3-padding-blog">

      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Error!</strong> Revise los campos obligatorios.<br><br>
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if(Session::has('success'))
      <div class="alert alert-info">
        {{Session::get('success')}}
      </div>
      @endif
          <div class="table-container">
            <form method="POST" action="{{ route('formacion.store') }}"  role="form" enctype="multipart/form-data">
              {{ csrf_field() }}
 
              <div class="form-group">
                <textarea name="convocatoria" id="convocatoria" class="form-control input-sm" placeholder="Convocatoria"></textarea>
              </div>
              <div class="form-group">
                    <textarea name="requisitos" id="requisitos" class="form-control input-sm" maxlength="2000" rows="5" placeholder="Requisitos"></textarea>
              </div>
              <p>Áreas de formación personal</p>

              {!! Form::select('area', $area, null, ['class' => 'form-control'] ) !!}
                <br>
                <p>Fecha de inicio</p>
                <div class="form-group">
                <input class="form-control input-sm" type="date" name="fecha1">
              </div>
              <p>Fecha de finalización</p>
                <div class="form-group">
                <input class="form-control input-sm" type="date" name="fecha2">
              </div>

              <div class="form-group">
                    <textarea name="lugar" id="lugar" class="form-control input-sm" maxlength="2000" rows="5" placeholder="Lugar"></textarea>
              </div>
              <div class="form-group">
                    <textarea name="responsable" id="responsable" class="form-control input-sm" maxlength="2000" rows="5" placeholder="Responsable"></textarea>
              </div>

              <br>

            <div class="form-group">
                <img width="50px" src="/img/subir_imagen.png">
                <input type="file" name="programaImg" id="programaImg" >
              </div>
              <div class="form-group">
               <img width="50px" src="/img/pdf.jpg">
                <input type="file" name="programapdf" class="form-control input-sm"  >
              </div>
              <div class="form-group">
                <img width="50px" src="/img/ppt.jpg">
                <input type="file" name="programappt" class="form-control input-sm"  >
              </div>
              <div class="form-group">
                 <img width="50px" src="/img/word.png">
                <input type="file" name="programapdfword" class="form-control input-sm"  >
              </div>
              <div class="form-group">
                <img width="50px" src="/img/excel.png">
                <input type="file" name="programapdfexcel" class="form-control input-sm"  >
              </div>
              <div class="form-group">
                <img width="50px" src="/img/video.jpg">
                <input type="file" name="programapdfvideo" class="form-control input-sm"  >
              </div>  
          <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
             <div class="captcha">
               <span>{!! captcha_img() !!}</span>

               </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
             <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha"></div>
          </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                  <input type="submit"  value="Guardar" class="btn btn-success btn-block">
                  <a href="{{ route('formacion.programa') }}" class="btn btn-info btn-block" > Atrás</a></div>
                </form>
          </div>

      </div>

<br>

   
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
<div class="w3-container">
</div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>

@endsection
