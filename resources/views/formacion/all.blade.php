@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">

      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
        <form method="GET" action="{{action('FormacionController@all')}}"  role="form" enctype="multipart/form-data">
              {{ csrf_field() }}
         <p>Buscar</p>
         
         <p>Fecha de inicio</p>
                <div class="form-group">
                <input class="form-control input-sm" type="date" name="fecha1">
              </div>
              <p>Fecha de finalización</p>
                <div class="form-group">
                <input class="form-control input-sm" type="date" name="fecha2">
              </div>
          
              <div class="col-xs-12 col-sm-12 col-md-12">
                  <input type="submit"  value="Buscar" class="w3-button w3-theme-d2 w3-margin-bottom">
                  </div>
         </form>
                           <div class="w3-container w3-padding">
          <button type="button" class="w3-button w3-block w3-theme-l4"><a href="{{ url('/formacion.area') }}" class="w3-button-up" title="Messages">Atrás</a></button>
        </div>
        </div>
      </div> 
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding .w3-padding-blog">
        @if($convocatorias->count())  
        @foreach($convocatorias as $convocatoria)
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container">
            <h2>{{ $convocatoria->  fecha_ini}}</h2>
            <i style="color:#BDBDBD;">{{ $convocatoria-> area }}</i>
            <h2><i>{{ $convocatoria->  convocatoria}}</i></h2>


          
          <a href="{{action('FormacionController@edit', $convocatoria->id)}}" ><i style="color: blue;"> Ver más +</i></a>
<br>
            </div>
          </div>
             <br>
        </div>
    @endforeach 
    @endif

      </div>

<br>

   
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
<div class="w3-container">
</div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>
<script type="text/javascript">
  var texto, padre;
$(".contenidoBlog").each(function(){
    texto = $(this).html();
    this.setAttribute("data-texto", texto);
    if ($(this).html().length > 75){
        $(this)
            .html(texto.substr(0, 75) + "...");
    }
});
 

</script>
@endsection
