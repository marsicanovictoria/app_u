@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->

      <br>   
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container w3-padding" >
    <div class="pricing-table pricing-three-column row">
          <div class="w3-container"  style="padding-bottom:32px;">
          <div class="plan-name-bronze">

            <h3>Área: <i>{{ $programa->  area}}</i></h3>
            <br>

  <div class="w3-container" style="padding-bottom:32px;">
      <div class="w3-third">
        <div class="w3-row w3-center w3-card w3-padding">
        <p>Fecha Inicio:</p> <p>{{ $programa->  fecha_ini}}</p>
      </div>
      </div>

      <div class="w3-third">
      <div class="w3-row w3-center w3-card w3-padding">  
        <p>Fecha Fin:</p> <p>{{ $programa->  fecha_fin}}</p>
      </div>
      </div>
      <div class="w3-third">
        <div class="w3-row w3-center w3-card w3-padding">
        <p>Responsable:</p> <p>{{ $programa->  responsable}}</p>
      </div>
      </div>
</div>

  <div class="w3-content" style="max-width:700px">
  
    <div class="w3-row w3-center w3-card w3-padding">
      <a href="javascript:void(0)" onclick="openMenu(event, 'Eat');" id="myLink">
        <div class="w3-col s6 tablink">Convocatoria</div>
      </a>
      <a href="javascript:void(0)" onclick="openMenu(event, 'Drinks');">
        <div class="w3-col s6 tablink">Requisito</div>
      </a>
    </div>

    <div id="Eat" class="w3-container menu w3-padding-48 w3-card">
      
      <p>{{ $programa->  convocatoria}}</p>
    </div>

    <div id="Drinks" class="w3-container menu w3-padding-48 w3-card">
      
      <p>{{ $programa->  requisito}}</p>
    </div>  
  </div>
  <br>
<p>Lugar: {{ $programa->  lugar}}</p>

    <br>
            
          </div>
@if($programa-> imagen != NULL)
          <div class="plan-name-bronze">
            <img width="70%" src="{{ Storage::url($programa->imagen) }}">
          </div>
@elseif($programa-> pdf != NULL)
        <a href="{{ action('FormacionController@pdf', $programa->id) }}">
            <img width="20px" src="/img/pdf.jpg">
        </a>
@elseif($programa-> ppt != NULL)
        <a href="{{ action('FormacionController@ppt', $programa->id) }}" >
            <img width="20px" src="/img/ppt.jpg">
        </a>
@elseif($programa-> word != NULL)
        <a href="{{ action('FormacionController@word', $programa->id) }}" >
            <img width="20px" src="/img/word.png">
        </a>
@elseif($programa-> excel != NULL)
        <a href="{{ action('FormacionController@excel', $programa->id) }}" >
            <img width="20px" src="/img/excel.png">
        </a>
@elseif($programa-> video != NULL)
        <a href="{{ action('FormacionController@video', $programa->id) }}" >
            <img width="20px" src="/img/video.jpg">
        </a>
@endif
        </div>
    </div>
            </div>
             <a href="{{ route('formacion.index') }}" class="w3-button-up" title="Messages"><span class="glyphicon glyphicon-backward">
        </span>Atrás</a>
          </div>
             <br>
        </div>


      </div>
        <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container w3-padding">
  <div class="w3-content" style="max-width:700px">
    <p>Comentarios</p>
    
<form method="POST" action="{{action('FormacionController@comentario', $programa->id)}}"  role="form">
              {{ csrf_field() }}
      <p><input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <p>Nombre</p>
        <p><input class="w3-input w3-padding-16 w3-border" type="text"  name="nombre" id="nombre" required value=""></p>
        <p>Apellido</p>
        <p><input class="w3-input w3-padding-16 w3-border" type="text"  name="apellido" id="apellido" required value=""></p>
        <p>Correo electrónico</p>
        <p><input class="w3-input w3-padding-16 w3-border" type="text"  name="mail" id="mail" required value=""></p>
        <p>Deja tu comentario</p>
        <input class="w3-input w3-padding-16 w3-border" type="text"  name="comentario" id="comentario" required value=""></p>
      <button class="w3-button w3-theme-d2 w3-margin-bottom" type="submit">Enviar</button></p>
    </form>

    </div>
            </div>
          </div>
        </div>
      </div>
   <br>
        @if($comen->count())  
@foreach($comen as $comen)
        <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container w3-padding">
  <div class="w3-content" style="max-width:700px">
  <p>Nombre: {{ $comen-> nombre}}</p>
  <p>Apellido: {{ $comen-> nombre}}</p>
  <p>E-mail:{{ $comen-> email}}</p>
  <p>Comentario:</p>
  <p>{{ $comen-> comentario}}</p>
  <p>{{ $comen-> created_at}}</p>


    </div>
            </div>
          </div>
        </div>

      </div>
      <br>
@endforeach 
@else
               
                <p>No hay comentarios !!</p>
            
              @endif      

            </div>
          </div>
        </div>

      <br>
 
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
<div class="w3-container">

</div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>

<script>
// Tabbed Menu
function openMenu(evt, menuName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("menu");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(menuName).style.display = "block";
  evt.currentTarget.firstElementChild.className += " w3-dark-grey";
}
document.getElementById("myLink").click();


var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 5000); // Change image every 2 seconds
}

</script>

@endsection
