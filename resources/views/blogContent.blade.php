@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->

      <br>   
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container w3-padding">
    <div class="pricing-table pricing-three-column row">
          <div class="w3-container"  style="padding-bottom:32px;">
          <h2>{{ $blog->  created_at->toFormattedDateString()}}</h2>
          <i style="color:#BDBDBD;">Ernesto Horna | Blog Oficial</i>
          <div class="plan-name-bronze">

            <h2><i>{{ $blog->  titulo}}</i></h2>
            <br>
            <p>{{ $blog->  contenido}}</p>
            <p>{{ $blog->  enlace}}</p>
          </div>
      @if($blog-> imagen != null )
          <div class="plan-name-bronze">
            <img width="70%" src="{{ Storage::url($blog->imagen) }}">
          </div>
          @endif
      @if($blog-> video !=null )    
        <a href="{{ action('BlogIndexController@video', $blog->id) }}" >
            <img width="20px" src="/img/video.jpg">
        </a>
        @endif
      @if($blog-> archivo != null)
        <a href="{{ action('BlogIndexController@pdf', $blog->id) }}">
            <img width="20px" src="/img/pdf.jpg">
        </a>
        @endif
      @if($blog-> ppt != null)
        <a href="{{ action('BlogIndexController@ppt', $blog->id) }}" >
            <img width="20px" src="/img/ppt.jpg">
        </a>
        @endif
      @if($blog-> docx != null)
        <a href="{{ action('BlogIndexController@docx', $blog->id) }}" >
            <img width="20px" src="/img/word.png">
        </a>
        @endif
      @if($blog-> xls != null)
        <a href="{{ action('BlogIndexController@xlsx', $blog->id) }}" >
            <img width="20px" src="/img/excel.png">
        </a>
      @endif

        </div>
    </div>
            </div>
          </div>
             <br>
        </div>


      </div>

<br>
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container w3-padding">
  <div class="w3-content" style="max-width:700px">
    <p>Comentarios</p>
    
<form method="POST" action="{{action('BlogIndexController@store', $blog->id)}}"  role="form">
              {{ csrf_field() }}
      <p><input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <input class="w3-input w3-padding-16 w3-border" type="text"  name="comentarios" id="comentarios" required value=""></p>
        <p>Nombre</p>
        <p><input class="w3-input w3-padding-16 w3-border" type="text"  name="nombre" id="nombre" required value=""></p>
        <p>Correo electrónico</p>
        <p><input class="w3-input w3-padding-16 w3-border" type="text"  name="email" id="email" required value=""></p>
      <button class="w3-button w3-theme-d2 w3-margin-bottom" type="submit">Enviar</button></p>
    </form>

    </div>
            </div>
          </div>
        </div>

      </div>
   <br>
   
   @if($comen->count())  
@foreach($comen as $comen)
      <div class="w3-row-padding">

        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container w3-padding">
  <div class="w3-content" style="max-width:700px">



  <p>{{ $comen-> nombre}}</p>
  <p>{{ $comen-> email}}</p>
  <p>{{ $comen-> comentario}}</p>
  <p>{{ $comen-> created_at}}</p>


    </div>
            </div>
          </div>
        </div>

      </div>
      <br>
@endforeach 
@else
               
                <p>No hay comentarios !!</p>
            
              @endif      
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
<div class="w3-container">
</div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>

@endsection
