@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
         <h4 class="w3-center"></h4>
        <form method="GET" action="{{action('BlogIndexController@show')}}"  role="form" enctype="multipart/form-data">
              {{ csrf_field() }}
         <p>Buscar por título</p>
         <p><input type="text" name="titulo" class="form-control input-sm"></p>
                         <div class="col-xs-12 col-sm-12 col-md-12">
                  <input type="submit"  value="Buscar" class="w3-button w3-theme-d2 w3-margin-bottom">
                  </div>
         </form>
         <p><i class="fa fa-birthday-cake fa-fw w3-margin-right w3-text-theme"></i></p>
        </div>
      </div>
      <br>   
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding .w3-padding-blog">
                <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container">
              {{  $contenido->  contenido }}
         </div>
         </div><br>
         </div>     
        @if($blog->count())  
        @foreach($blog as $blogs)
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container">
            <h1>{{ $blogs->  created_at->toFormattedDateString()}}</h1>
            <i style="color:#BDBDBD;">Ernesto Horna | Blog Oficial</i>
            <h2><i>{{ $blogs->  titulo}}</i></h2>
            <div class = "contenidoBlog">
    {{ $blogs->  contenido}}
</div>

          
          <a href="{{action('BlogIndexController@edit', $blogs->id)}}" ><i style="color: blue;"> Ver más +</i></a>
<br>
            </div>
          </div>
             <br>
        </div>
    @endforeach 
    @endif

      </div>

<br>

   
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
<div class="w3-container">
</div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>
<script type="text/javascript">
  var texto, padre;
$(".contenidoBlog").each(function(){
    texto = $(this).html();
    this.setAttribute("data-texto", texto);
    if ($(this).html().length > 75){
        $(this)
            .html(texto.substr(0, 75) + "...");
    }
});
 

</script>
@endsection
