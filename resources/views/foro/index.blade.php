@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
         <h4 class="w3-center"></h4>

         <p><i class="fa fa-birthday-cake fa-fw w3-margin-right w3-text-theme"></i></p>
        </div>

      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
          <button type="button" class="w3-button w3-block w3-theme-l4">
            <a href="{{ route('foro.activo') }}" class="w3-button-up" title="Messages">Abiertos</a></button>
          <button type="button" class="w3-button w3-block w3-theme-l4">
            <a href="{{ route('foro.cerrado') }}" class="w3-button-up" title="Messages">Cerrados</a></button>
          <button type="button" class="w3-button w3-block w3-theme-l4">
            <a href="{{ route('foro.create') }}" class="w3-button-up" title="Messages">Crear evento</a></button>

        </div>
      </div>    

      </div>
      <br>   
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding .w3-padding-blog">
        @if($foro->count())  
        @foreach($foro as $foros)
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container"
            @if($foros->activo == 1) style="background: #fff;"
            @elseif($foros->activo == 0) style="background: #fff;"
            @endif>
            <h2>{{ $foros->  created_at}}</h2>
            <i style="color:#BDBDBD;">{{ $foros-> name }}</i>
            <h2><i>{{ $foros->  titulo}}</i></h2>
            <p><i>Eje: {{ $foros->  eje}}</i></p>
            @if($foros->foro == 1)
              <p><i>Tipo Evento: Foro</i></p>
            @elseif($foros->foro == 0)
          <p><i>Tipo Evento: Chat</i></p>
          @endif

          <a href="{{action('ForoController@show', $foros->id)}}" ><i style="color: blue;"> Ver más +</i></a>
<br>
            </div>
          </div>
             <br>
        </div>
    @endforeach 
    @endif

      </div>

<br>

   
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
<div class="w3-container">
</div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>
<script type="text/javascript">
  var texto, padre;
$(".contenidoBlog").each(function(){
    texto = $(this).html();
    this.setAttribute("data-texto", texto);
    if ($(this).html().length > 75){
        $(this)
            .html(texto.substr(0, 75) + "...");
    }
});
 

</script>
@endsection
