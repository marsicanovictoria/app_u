@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->

      <br>   
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container w3-padding">
    <div class="pricing-table pricing-three-column row">
      <div class="w3-container"  style="padding-bottom:32px;">
      @if($foro->id_usuario == auth()->user()->id && $foro->activo == 1)    
          
          <a href="{{route('foro.update', $foro->id)}}" class="w3-button-up" title="Messages">Cerrar evento</a>
      @endif
          <h2>{{ $foro->  created_at}}</h2>
          <i style="color:#BDBDBD;">{{ $foro-> name}}</i>
          <div class="plan-name-bronze">

            <h2><i>{{ $foro->  titulo}}</i></h2>
            <br>
            <p>{{ $foro->  contenido}}</p>
            <p>{{ $foro->  eje}}</p>
            @if($foro->foro == 1)
              <p><i>Tipo Evento: Foro</i></p>
            @elseif($foro->foro == 0 && $foro->activo == 1)
          <p><i>Tipo Evento: Chat</i></p>
            <a href="{{action('ForoController@sala', $foro->id)}}" ><i style="color: blue;"> Chat {{ $foro->  titulo}} </i></a>
          @endif
          </div>

        </div>

            <a href="{{ route('foro.index') }}" class="w3-button-up" title="Messages"><span class="glyphicon glyphicon-backward">
        </span>Atrás</a>
    </div>
            </div>
          </div>
             <br>
        </div>


      </div>

<br>
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container w3-padding">
  <div class="w3-content" style="max-width:700px">
    <p>Respuestas</p>
    
<form method="POST" action="{{action('ComentarioForoController@store', $foro->id)}}"  role="form">
              {{ csrf_field() }}
      <p><input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <input class="w3-input w3-padding-16 w3-border" type="text"  name="comentario" id="comentario" required value=""></p>
      <button class="w3-button w3-theme-d2 w3-margin-bottom" type="submit">Enviar</button></p>
    </form>

    </div>
            </div>
          </div>
        </div>

      </div>
   <br>
   
   @if($forocomen->count())  
@foreach($forocomen as $forocomen)
      <div class="w3-row-padding">

        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container w3-padding">
  <div class="w3-content" style="max-width:700px">



  <p>{{ $forocomen-> name}}</p>
  <p>{{ $forocomen-> comentario}}</p>
  <p>{{ $forocomen-> created_at}}</p>


    </div>
            </div>
          </div>
        </div>

      </div>
      <br>
@endforeach 
@else
               
                <p>No hay comentarios !!</p>
            
              @endif      
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
<div class="w3-container">
</div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>

@endsection
