<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{mix('css/app.css')}}" rel="stylesheet" type="text/css">
    <script src="Chart.min.js"></script>
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
            <!-- vinculo a bootstrap -->

                <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('css/bootstrap-theme.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('js/nicEdit.js') }}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/scripts.js"></script>
<!-- Temas-->

<!-- se vincula al hoja de estilo para definir el aspecto del formulario de login-->  
<link rel="stylesheet" type="text/css" href="estilo.css">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
                    <div class="w3-top">
 <div class="w3-bar w3-theme-d2 w3-left-align w3-large">
  <a href="/" class="w3-bar-item w3-padding-large"><img src="/img/logo_app.png" style="width:50%" class="w3-margin-bottom"></a>
 </div>
 <div class="w3-bar-up w3-theme-d2-up w3-left-align-up w3-large-up">
    <div class="w3-button-up">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            
                                <a class="w3-button-up2" href="{{ route('login') }}">{{ __('ENTRAR') }}</a>
                            
                                @if (Route::has('register'))
                                    <a class="w3-button-up2" href="{{ route('register') }}">{{ __('REGISTRAR') }}</a>
                                @endif
                            
                        @else
                            
                                <a class="w3-button-up2">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                    <a class="w3-button-up2" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('SALIR') }}
                                    </a>
                                      @if( (Auth::user()->admin) == 1 )
  <a href="{{ url('/administracion')}}" class="w3-button-up" title="Messages">ADMINISTRACIÓN</a>@endif
  @if( (Auth::user()->admin) == 0 )
  <a href="{{ url('/configUsu')}}" class="w3-button-up" title="Messages">CONFIGURACIÓN</a>
  @csrf
  @endif

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                
                            
                        @endguest
                    </ul>
                </div>
            </div>
    <a href="{{ url('/home')}}" class="w3-button-up" title="Comunicaciones">PRESENTACIÓN</a>
    <a href="{{ url('/formacion')}}" class="w3-button-up" title="Account Settings">FORMACIÓN PERSONAL</a>
    <a href="{{ url('/blogIndex')}}" class="w3-button-up" title="Messages">BLOG OFICIAL</a>
    <a href="{{ url('/consultas') }}" class="w3-button-up" title="Messages">CONSULTAS</a>

  <a href="{{ url('/arte') }}" class="w3-button-up" title="Messages">ARTE</a>

   
        </div>

 <div class="w3-bar-up w3-theme-d3-up w3-left-align-up w3-large-up">
<a href="{{ url('/comunicacionIndex') }}" class="w3-button-up" title="Comunicaciones">COMUNICACIONES</a>
  <a href="{{ url('/noticia') }}" class="w3-button-up" title="Account Settings">NOTICIAS</a>
  <a href="{{ url('/investigacion') }}" class="w3-button-up" title="Messages">INVESTIGACIONES</a>
  <a href="#" class="w3-button-up" title="Messages">ESTUDIOS</a>
  <a href="#" class="w3-button-up" title="Messages">TESIS</a>
  <a href="#" class="w3-button-up" title="Messages">CURSOS</a>
  <a href="{{ url('/encuesta') }}" class="w3-button-up" title="Messages">ENCUESTAS Y TENDENCIAS SOCIALES</a>

</div>

</div>
            </div>

        <main class="py-4">
            @yield('content')
        </main>

</body>
</html>