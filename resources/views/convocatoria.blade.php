@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
    </br> 
      <div class="w3-card w3-round w3-white">
      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
        <form method="GET" action=""  role="form" enctype="multipart/form-data">
              {{ csrf_field() }}
         <p><b>POSTULA PARA CONSULTOR</b></p>
<br>
         <p>Los campos marcados con un * son obligatorios</p>
         <br>
         <p>Nombre</p>
         <p><input type="text" name="nombre" class="form-control input-sm"></p>
         <p>Apellido</p>
         <p><input type="text" name="apellido" class="form-control input-sm"></p>
         <p>Email</p>
         <p><input type="text" name="email" class="form-control input-sm"></p>
         <p>Dejanos tu mensaje*</p>
         <p><textarea name="mensaje" class="form-control input-sm"></textarea></p>
                         <div class="col-xs-12 col-sm-12 col-md-12">
                  <input type="submit"  value="Enviar" class="w3-button w3-theme-d2 w3-margin-bottom">
                  </div>
         </form>

        </div>
      </div>    

      </div> 
      <br>   
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding .w3-padding-blog">

<div class="w3-container" id="presentacion">
  <div class="w3-content" style="max-width:700px">
  
    <div class="w3-row w3-center w3-card w3-padding">
      <a href="javascript:void(0)" onclick="openMenu(event, 'convocatoria');" id="myLink">
        <div class="w3-col s6 tablink">CONVOCATORIA</div>
      </a>
      <a href="javascript:void(0)" onclick="openMenu(event, 'requisito');">
        <div class="w3-col s6 tablink">REQUISITOS</div>
      </a>
    </div>

    <div id="convocatoria" class="w3-container menu w3-padding-48 w3-card">
      
      <p>Se convoca a inscribirse a personal experto en consultoria, enviando una carta en la que manifieste su deseo de participar en la Consultoría que la red social brindará a los universitarios. Esta convocatoria está dirigida preferentemente a profesionales expertos en Gerencia, Liderazgo,Comportamiento Organizacional, Solución de Conflictos Sociales, Solución de conflictos personales, Proyectos, Finanzas,Investigadores Científicos, etc. Indicando el área de su experticia mencionando explícitamente su experiencia en dicho campo. </p><br>
    </div>

    <div id="requisito" class="w3-container menu w3-padding-48 w3-card">
      
      <p>1. Compartir los fundamentos conceptuales y fácticos que sustentan la necesidad de una formación integral del estudiante universitario y ver en la Formación Personal una opción viable a dicho propósito.</p><br>

      <p>2. Deberá sentir un profundo amor a la educación, docencia y a la juventud estudiosa, superior alinterés económico y encontrar en la Consultoría una forma de comunicarse, de contribuir a su fortalecimiento personal y de ofrecer su aporte a la educación de los universitarios, a través del aprendizaje de la solución de sus problemas.</p><br>

      <p>3. Poseer el Grado Academico de Magister o Doctor vinculado al campo de su experticia.</p><br>

      <p>4. Acreditar experticia en el campo al que postula.</p><br>

      <p>5. Enviar currículo no documentado a la siguiente dirección: <i><b>presidencia@universitariosunios.com</b></i></p><br>

      <p>Autorizar, en caso de ser seleccionado, a su nombre en las gestiones para el financiamiento de la implementación y difusión del Servicio a ofrecer.</p><br>
    </div>  
  </div>
</div>

      </div>

<br>

   
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
<div class="w3-container">
</div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>
<script>
// Tabbed Menu
function openMenu(evt, menuName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("menu");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-dark-grey", "");
  }
  document.getElementById(menuName).style.display = "block";
  evt.currentTarget.firstElementChild.className += " w3-dark-grey";
}
document.getElementById("myLink").click();


var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 5000); // Change image every 2 seconds
}

</script>
@endsection
