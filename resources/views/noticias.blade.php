@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
         <h4 class="w3-center"></h4>
         <p><i class="fa fa-birthday-cake fa-fw w3-margin-right w3-text-theme"></i></p>
        </div>

      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
            <button type="button" class="w3-button w3-block w3-theme-l4">
            <a href="{{ url('noticia.index') }}" class="w3-button-up" title="Messages">Todas las noticias</a></button>
          <button type="button" class="w3-button w3-block w3-theme-l4">
            <a href="{{ route('noticia.create') }}" class="w3-button-up" title="Messages">Nueva +</a></button>
          <button type="button" class="w3-button w3-block w3-theme-l4">
            <a href="{{  route('noticia.mis_noticias') }}" class="w3-button-up" title="Messages">Mis noticias</a></button>
        </div>
      </div>    

      </div>
      <br>   
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding .w3-padding-blog">
        
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container">
            <p>{{ $contenido -> contenido }}</p>
            </div>
          </div>
             <br>
        </div>


      </div>

<br>

   
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
<div class="w3-container">
</div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>
<script type="text/javascript">
  var texto, padre;
$(".contenidoBlog").each(function(){
    texto = $(this).html();
    this.setAttribute("data-texto", texto);
    if ($(this).html().length > 75){
        $(this)
            .html(texto.substr(0, 75) + "...");
    }
});
 

</script>
@endsection
