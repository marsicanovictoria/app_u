

@extends('layouts.app')

@section('content')

<!-- Navbar on small screens -->

<!-- Page Container -->
<div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:8%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->

      <br>
      
     

      <br>
      
      <!-- Alert Box -->
    
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div class="w3-container w3-padding">

      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Error!</strong> Revise los campos obligatorios.<br><br>
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if(Session::has('success'))
      <div class="alert alert-info">
        {{Session::get('success')}}
      </div>
      @endif
 
        
          <div class="table-container">
            <form method="POST" action="{{ route('comunicacionIndex.storeComunicacion') }}"  role="form">
              {{ csrf_field() }}
 
              <div class="form-group">
                <textarea name="titulo" id="titulo" class="form-control input-sm" placeholder="Título"></textarea>
              </div>
              <div class="form-group">
                    <textarea name="comunicacion" id="comunicacion" class="form-control input-sm" maxlength="2000" rows="5" placeholder="Comunicación"></textarea>
              </div>
              <p>Eje de la actividad académaica universitaria</p>

              {!! Form::select('eje', $eje, null, ['class' => 'form-control'] ) !!}

              <br>
 
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <input type="submit"  value="Guardar" class="btn btn-success btn-block">
                  <a href="" class="btn btn-info btn-block" > Atrás</a></div>
            </form>

 

          </div>
        </div>
          </div>
        </div>
        
      </div>
      
    <!-- End Middle Column -->
    </div>
    

    
  <!-- End Grid -->
  </div>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
          


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!-- End Page Container -->
</div>
<br>
 
</html> 
