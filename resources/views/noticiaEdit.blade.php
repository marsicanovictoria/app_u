@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
      <div class="w3-card w3-round w3-white">


      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
          
        </div>
      </div>    

      </div>
      <br>   
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding .w3-padding-blog">

      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Error!</strong> Revise los campos obligatorios.<br><br>
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if(Session::has('success'))
      <div class="alert alert-info">
        {{Session::get('success')}}
      </div>
      @endif
          <div class="table-container">
            <form method="POST" action="{{ action('NoticiasController@update', $noticias->id) }}"  role="form">
              {{ csrf_field() }}
 
              <div class="form-group">
                <textarea name="titulo" id="titulo" class="form-control input-sm" placeholder="Título">{{$noticias->titulo}}</textarea>
              </div>
              <div class="form-group">
                    <textarea name="contenido" id="contenido" class="form-control input-sm" maxlength="2000" rows="5" placeholder="Contenido">{{$noticias->contenido}}</textarea>
              </div>
              <div class="form-group">
                    <textarea name="entidad" id="entidad" class="form-control input-sm" maxlength="100" placeholder="Nombre entidad">
                      {{$noticias->entidad}}
                    </textarea>
              </div>
              <div class="form-group">
                    <textarea name="carrera" id="carrera" class="form-control input-sm" maxlength="100" placeholder="Carrera">
                      {{$noticias->carrera}}
                    </textarea>
              </div>
 
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <input type="submit"  value="Guardar" class="btn btn-success btn-block">
                  <a href="" class="btn btn-info btn-block" > Atrás</a></div>
            </form>
          </div>

      </div>

<br>

   
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
<div class="w3-container">
</div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>

@endsection
