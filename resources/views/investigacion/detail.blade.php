@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->

      <br>   
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container w3-padding" style="background: #cce6ff">
    <div class="pricing-table pricing-three-column row">
          <div class="w3-container"  style="padding-bottom:32px;">
          <h2>{{ $inv->  created_at->toFormattedDateString()}}</h2>
          
          <div class="plan-name-bronze">

            <h2><i>{{ $inv->  titulo}}</i></h2>
            <br>
            <p>{{ $inv->  descripcion}}</p>
          </div>
          @if($inv-> invpdf != NULL)
          <a target="_blank" href="{{action('InvestigacionsController@verPdf', $inv->id )}}">Ver: <i>{{ $inv->  titulo}}</i></a>
@elseif($inv-> invexcel != NULL)
        <a href="{{ action('InvestigacionsController@verEcxel', $inv->id) }}" >
            Ver: <i>{{ $inv->  titulo}}</i></a>
        </a>
@elseif($inv-> invsps != NULL)
        <a href="{{ action('InvestigacionsController@verSPSS', $inv->id) }}" >
            Ver: <i>{{ $inv->  titulo}}</i></a>
        </a>
@endif


        </div>
    </div>
            </div>
          </div>
             <br>
        </div>


      </div>

<br>
        <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container w3-padding">
  <div class="w3-content" style="max-width:700px">
    <p>Comentarios</p>
    
<form method="POST" action="{{action('InvestigacionsController@comentario', $inv->id)}}"  role="form">
              {{ csrf_field() }}
      <p><input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <p>Nombre</p>
        <p><input class="w3-input w3-padding-16 w3-border" type="text"  name="nombre" id="nombre" required value=""></p>
        <p>Apellido</p>
        <p><input class="w3-input w3-padding-16 w3-border" type="text"  name="apellido" id="apellido" required value=""></p>
        <p>Correo electrónico</p>
        <p><input class="w3-input w3-padding-16 w3-border" type="text"  name="email" id="email" required value=""></p>
        <p>Deja tu comentario</p>
        <input class="w3-input w3-padding-16 w3-border" type="text"  name="comentario" id="comentario" required value=""></p>
      <button class="w3-button w3-theme-d2 w3-margin-bottom" type="submit">Enviar</button></p>
    </form>

    </div>
            </div>
          </div>
        </div>
      </div></div>
         <br>
   
   @if($c->count())  
@foreach($c as $c)
      <div class="w3-row-padding">

        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container w3-padding">
  <div class="w3-content" style="max-width:700px">



  <p>{{ $c-> nombre}}</p>
  <p>{{ $c-> email}}</p>
  <p>{{ $c-> comentario}}</p>
  <p>{{ $c-> created_at}}</p>


    </div>
            </div>
          </div>
        </div>

      </div>
      @endforeach 
@else
              </div>
              <div>
                <p>No hay comentarios !!</p>
            
              @endif  
              </div>
    <!-- End Middle Column -->
    </div>
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
<div class="w3-container">
</div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>

@endsection
