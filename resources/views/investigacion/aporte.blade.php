@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
         <h4 class="w3-center"></h4>
         <p><i class="fa fa-birthday-cake fa-fw w3-margin-right w3-text-theme"></i></p>
        </div>

      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
              <button type="button" class="w3-button w3-block w3-theme-l4"><a href="{{  route('investigacion.show') }}" class="w3-button-up" title="Messages">Todas las Investigaciones</a></button>
          
              <button type="button" class="w3-button w3-block w3-theme-l4"><a href="{{  route('investigacion.create') }}" class="w3-button-up" title="Messages">Nueva Investigación</a></button>
              <button type="button" class="w3-button w3-block w3-theme-l4"><a href="{{  route('investigacion.aporte') }}" class="w3-button-up" title="Messages">Panel de Entrada</a></button>
        </div>
      </div>    

      </div>
      <br>   
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding .w3-padding-blog">
        
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container">
              <table class="w3-table-all">
              <tr>
                <th>Titulo</th>
                <th>Fecha</th>
                <th></th>
                <th></th>
              </tr>
              @if($inv->count())  
              @foreach($inv as $invs)
              <tr>
              <td>{{ $invs-> titulo }}</td>
              <td>{{ $invs-> created_at }}</td>
              <td>
                      <a class="btn btn-primary btn-xs" class="btn btn-success btn-xs" href="{{action('InvestigacionsController@edit_aporte', $invs->id)}}" type="submit"><span class="glyphicon glyphicon-pencil">
                      </span>
                    </a>
                  </td>
                  <td>
                   <form action="{{action('InvestigacionsController@destroy', $invs->id)}}" method="POST">
                    {{csrf_field()}}
                      <input name="_method" type="hidden" value="DELETE">
                      <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash">
                      </span>
                    </button>
                  </td>
              </tr>
              @endforeach 
                             @else
                             <tr>
                              <td colspan="8">No hay registro !!</td>
                            </tr>
                            @endif
              </table>
            </div>
          {{ $inv->links() }}
          </div>
             <br>
        </div>


      </div>

<br>

   
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
<div class="w3-container">
</div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>

@endsection
