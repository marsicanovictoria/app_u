@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
         <h4 class="w3-center"></h4>
         <p><i class="fa fa-birthday-cake fa-fw w3-margin-right w3-text-theme"></i></p>
        </div>

      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
              <button type="button" class="w3-button w3-block w3-theme-l4"><a href="{{  route('investigacion.show') }}" class="w3-button-up" title="Messages">Todas las Investigaciones</a></button>
          
              <button type="button" class="w3-button w3-block w3-theme-l4"><a href="{{  route('investigacion.create') }}" class="w3-button-up" title="Messages">Nueva Investigación</a></button>
              <button type="button" class="w3-button w3-block w3-theme-l4"><a href="{{  route('investigacion.aporte') }}" class="w3-button-up" title="Messages">Panel de Entrada</a></button>
        </div>
      </div>    

      </div>
      <br>   
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding .w3-padding-blog">
        
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container">
            <form method="POST" action="{{ action('InvestigacionsController@update_aporte', $inv->id) }}"  role="form" enctype="multipart/form-data">
              {{ csrf_field() }}
                <p>Título</p>
              <div class="form-group">
                <textarea name="titulo" id="titulo" class="form-control input-sm" placeholder="Título">{{ $inv->titulo }}</textarea>
              </div>
              <p>Descripción</p>
              <div class="form-group">
                <textarea name="descripcion" id="descripcion" class="form-control input-sm" maxlength="3000" rows="10" placeholder="Descripción">{{ $inv->descripcion }}</textarea>
              </div>
            
 
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <input type="submit"  value="Guardar" class="btn btn-success btn-block">
                  <a href="" class="btn btn-info btn-block" > Atrás</a></div>
                             
            </form>
            </div>
          
          </div>
             <br>
        </div>


      </div>

<br>

   
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
<div class="w3-container">
</div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>

@endsection

