

  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{mix('css/app.css')}}" rel="stylesheet" type="text/css">
    <script src="Chart.min.js"></script>
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
            <!-- vinculo a bootstrap -->

                <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('css/bootstrap-theme.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('js/nicEdit.js') }}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/scripts.js"></script>
<!-- Temas-->

<!-- se vincula al hoja de estilo para definir el aspecto del formulario de login-->  
<link rel="stylesheet" type="text/css" href="estilo.css">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <!-- /* INCLUYE LAS LIBRERÍAS NECESARIAS */ -->
        <script type="text/javascript" src="js/jquery-2.2.0.js"></script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
      
        <!-- Incluye los scripts (funciones)-->
               <style>  
            #nuevo-usuario{
                text-align:center;
                width:100%;
                display:none;
            }
            table tr td{
                text-align:right;
            }
            #nombre-usuario{                
                min-height:30px;
                background-color:white;
                text-align:center;
                position: fixed;
                z-index:+10000000;
                left:2em;
                top:1em;
                display:inline-block;
                display:none;
                border-radius:7px;
                box-shadow: 0px 0px 3px 3px lime;
            }
        </style>        
        <script>
            $(document).ready(function(){
                $('#sala-chat').hide();
                $('body').before(function(){ 
                    
                    //borrarDB()
                    crearTabla();
                }); 
                $('#nota').fadeIn('slow');
                    document.getElementById('nota').innerHTML='Debe crear un usuario para ingresar al chat';              
                $('#botonis').click(function(){
                    validar_campos(document.getElementById('formulario'));
                });
                $('#botonnu').click(function(){
                    $('#nuevo-usuario').fadeIn('smooth');
                });
                $('#botoncu').click(function(){
                    validar_campos(document.getElementById('nuevo-usuario'));
                });
                $('#botonOut').click(function(){
                    adios();
                });
                $('#botonmn').click(function(){
                    enviarMensaje(document.getElementById('mensaje-nuevo'));
                });
                $('#contenedor-chat').mouseenter(function(){
                    mensajeVisto();
                });
            });
        </script>

    <body onbeforeunload="adios()" onunload="adios()"> 
    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">       
        <div id="iniciar-sesion">
            <form id="formulario" method="post" action="chat.html">
                <table align="center">
                    <tbody>
                        <tr>
                            <td colspan="2" style="text-align:center;"><label for="Contraseña" class="col-md-12 col-form-label text-md-right">{{ __('Iniciar Sesión') }}</label></td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp
                            </td>
                            <td>
                                &nbsp
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="Usuario" class="col-md-4 col-form-label text-md-right">{{ __('Usuario') }}</label>
                            </td>
                            <td>
                                <input type="text" name="usuario" id="usuario" class="form-control" onfocus="fondo_blanco(this)">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp
                            </td>
                            <td>
                                &nbsp
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="Contraseña" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>
                            </td>
                            <td>
                                <input type="password" name="contraseña" id="contrasenia" class="form-control" onfocus="fondo_blanco(this)">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align:center;">
                                <br>
                                <br>
                                <br>
                                <input type="button" value="Iniciar Sesión" class="w3-button w3-theme-d2 w3-margin-bottom" id="botonis">
                                &nbsp
                                &nbsp
                                &nbsp
                                <input type="button" value="Nuevo Usuario" class="w3-button w3-theme-d2 w3-margin-bottom" id="botonnu">
                            </td>
                        <a href="{{ route('foro.index') }}" class="w3-button-up" title="Messages"><span class="glyphicon glyphicon-backward">
        </span>Atrás</a>
                            <div id="nota"></div>
                        </tr>
                    </tbody>
                </table>
            </form>
            
            <div id="nuevo-usuario">
                <table align="center">
                    <tbody>
                        <tr>
                            
                            <td>
                                <br>
                                <label for="Nombre" class="col-md-12 col-form-label text-md-right">{{ __('Nombre') }}</label></td>
                            <td><input type="text" name="nombre" class="form-control" id="n-nombre" required></td>
                        </tr>
                        <tr>
                            
                        <td>
                            <br><label for="Usuario" class="col-md-12 col-form-label text-md-right">{{ __('Usuario') }}</label></td>
                            <td><input type="text" name="nickname" class="form-control" id="n-nickname" required></td>
                        </tr>
                        <tr>
                            
                            <td>
                                <br><label for="Password" class="col-md-12 col-form-label text-md-right">{{ __('Password') }}</label></td>
                            <td><input type="password" name="password" class="form-control" id="n-password" required></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align:center">
                                <br>
                                <input type="button" class="w3-button w3-theme-d2 w3-margin-bottom" value="Crear usuario" id="botoncu">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="nombre-usuario"></div>
            <center>
                <div class="mensaje-validar" id="msg"></div>
            </center>            
        </div>  
        <div class="w3-col m7">
    
      <div class="w3-row-padding .w3-padding-blog">
        <div class="table-container">
        <div id="sala-chat">
            <div class="barra-superior" id="barra-usuario">
                <div id="nombre-usuario1"></div>
                <input type="button" class="boton-cerrar" value="Cerrar Sesión" id="botonOut">
            </div>
            <div id="contenedor-chat">
                <div id="usuarios-online"></div>
                <br>
                <div id="contenedor-mensajes"></div>
                <div id="contenedor-mensaje-nuevo">
                    <br>
                    <textarea id="mensaje-nuevo" name="mensaje" class="texto-mensaje"></textarea>
                    <br>
                    <br>
                    <input type="button" id="botonmn" value="Enviar" class="w3-button w3-theme-d2 w3-margin-bottom">
                </div>
            </div>
        </div>
        </div>
        </div>
        </div>
        <div class="pie">
        </div>
    </div>
    </body>
