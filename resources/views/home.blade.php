@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
         <h4 class="w3-center">Pensamientos</h4>
        @if($presentacion->count())  
        @foreach($presentacion as $presentacion)
         <hr>
         <p><i class="fa fa-pencil fa-fw w3-margin-right w3-text-theme"></i></p>
         <p><i class="fa fa-home fa-fw w3-margin-right w3-text-theme">{{ $presentacion-> pensamiento}}</i> </p>
         <p><i class="fa fa-birthday-cake fa-fw w3-margin-right w3-text-theme"></i></p>
        </div>
      </div>
      <br>
      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
          <button type="button" class="w3-button w3-block w3-theme-l4">
            <a href="#presentacion" class="w3-button-up" title="Messages">Presentación</a></button>
          <button type="button" class="w3-button w3-block w3-theme-l4"><a href="#proposito" class="w3-button-up" title="Messages">Propósito</a></button>
          <button type="button" class="w3-button w3-block w3-theme-l4"><a href="#estructura" class="w3-button-up" title="Messages">Estructura</a></button>
        </div>
      </div>    
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div id="presentacion" class="w3-container w3-padding">
  <div class="w3-content" style="max-width:700px">
    <h5 class="w3-center"><span class="w3-tag w3-wide">BIENVENIDOS A LA RED SOCIAL UNIVERSITARIOS UNÍOS</span></h5>
    

  <div class="w3-quarter" style="width:50%">
      <img src="/img/UUlog.png" class="img-responsive center-block
      " style="width:70%">
  </div>
  <div class="w3-quarter" style="width:50%">
      <p>{{ $presentacion-> presentacion}}</p>
  </div>

    </div>
            </div>
          </div>
        </div>

      </div>

<div id= "proposito" class="w3-container w3-card w3-white w3-round w3-margin">
<div class="w3-container"  style="padding-bottom:32px;">
  <h5 class="w3-center">
  <span class="w3-tag w3-wide">Propósitos</span></h5>
  <div class="w3-quarter" style="width:50%">
      <img src="/img/comu.png" class="img-responsive center-block
      " style="width:70%">
  </div>
  <div class="w3-quarter" style="width:50%">
      <p>{{ $presentacion-> proposito}}</p>
  </div>
</div>

<div class="w3-container"  style="padding-bottom:32px;">
  <div class="w3-quarter" style="width:50%">
      <p>{{ $presentacion-> proposito2}}</p>
  </div>
    <div class="w3-quarter" style="width:50%">
      <img src="/img/formacion.png" class="img-responsive center-block" style="width:70%;">
  </div>
</div>

<div class="w3-container" style="padding-bottom:32px;">
  <div class="w3-quarter" style="width:50%">
      <img src="/img/FOTO-UPV.jpg" class="img-responsive center-block" style="width:70%;">
  </div>
  <div class="w3-quarter" style="width:50%">
      <p>{{ $presentacion-> proposito3}}</p>
  </div>
</div>
</div>

@endforeach 
@endif

<div id= "estructura" class="w3-container w3-card w3-white w3-round w3-margin">
<div class="w3-container" style="padding-bottom:32px;">
  <h5 class="w3-center">
  <span class="w3-tag w3-wide">Estructura</span></h5>
      <div class="w3-third">
        <img src="/img/presentacion.png" class="w3-round w3-margin-bottom" alt="Random Name" style="width:60%">
        <p>Presentación.</p>
      </div>
      <div class="w3-third">
        <img src="/img/noticia.png" class="w3-round w3-margin-bottom" alt="Random Name" style="width:60%">
        <p>Noticias y Eventos.</p>
      </div>
      <div class="w3-third">
        <img src="/img/video-turorial.png" class="w3-round w3-margin-bottom" alt="Random Name" style="width:60%">
        <p>Programa de Formación Personal.</p>
      </div>
</div>
<div class="w3-container" style="padding-bottom:32px;">
      <div class="w3-third">
        <img src="/img/archivos.png" class="w3-round w3-margin-bottom" alt="Random Name" style="width:60%">
        <p>Investigaciones.</p>
      </div>
      <div class="w3-third">
        <img src="/img/comunicacion.png" class="w3-round w3-margin-bottom" alt="Random Name" style="width:60%">
        <p>Comunicaciones.</p>
      </div>
      <div class="w3-third">
        <img src="/img/sitio-web.png" class="w3-round w3-margin-bottom" alt="Random Name" style="width:60%">
        <p>Blog del DR. EHF.</p>
      </div>
</div>

<div class="w3-container" style="padding-bottom:32px;">
      <div class="w3-third">
        <img src="/img/informacion.png" class="w3-round w3-margin-bottom" alt="Random Name" style="width:60%">
        <p>Consultas.</p>
      </div>
      <div class="w3-third">
        <img src="/img/diploma-con-una-cinta.png" class="w3-round w3-margin-bottom" alt="Random Name" style="width:60%">
        <p>Estudios y Propuestas.</p>
      </div>
      <div class="w3-third">
        <img src="/img/tesis.png" class="w3-round w3-margin-bottom" alt="Random Name" style="width:60%">
        <p>Tesis y Libros.</p>
      </div>
</div>

<div class="w3-container" style="padding-bottom:32px;">
      <div class="w3-third">
        <img src="/img/tablet-tactil.png" class="w3-round w3-margin-bottom" alt="Random Name" style="width:60%">
        <p>Cursos.</p>
      </div>
      <div class="w3-third">
        <img src="/img/arte.png" class="w3-round w3-margin-bottom" alt="Random Name" style="width:60%">
        <p>Artes.</p>
      </div>
      <div class="w3-third">
        <img src="/img/test-online.png" class="w3-round w3-margin-bottom" alt="Random Name" style="width:60%">
        <p>Encuestas.</p>
      </div>
</div>
</div>
<div id= "estructura" class="w3-container w3-card w3-white w3-round w3-margin">
    <div class="w3-content" style="max-width:700px">
    <p>Comentarios</p>
    
    <form method="POST" action="{{ route('home.store') }}"  role="form">
              {{ csrf_field() }}
      <p><input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <input class="w3-input w3-padding-16 w3-border" type="text"  name="comentarios" id="comentarios" required value=""></p>
        <p>Nombre</p>
        <p><input class="w3-input w3-padding-16 w3-border" type="text"  name="nombre" id="nombre" required value=""></p>
        <p>Correo electrónico</p>
        <p><input class="w3-input w3-padding-16 w3-border" type="text"  name="email" id="email" required value=""></p>
      <button class="w3-button w3-theme-d2 w3-margin-bottom" type="submit">Enviar</button></p>
    </form>
  </div>
  
</div>      
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
<div class="w3-container">
</div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>

@endsection
