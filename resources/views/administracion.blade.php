@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:12%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
         <h4 class="w3-center">Publicaciones Recientes</h4>        
         <hr>
         <p><i class="fa fa-pencil fa-fw w3-margin-right w3-text-theme"></i></p>
         <p><i class="fa fa-home fa-fw w3-margin-right w3-text-theme"></i> </p>
         <p><i class="fa fa-birthday-cake fa-fw w3-margin-right w3-text-theme"></i></p>
        </div>
      </div>
      <br>   
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding .w3-padding-blog">
<h6 class="w3-opacity">Usuarios Registrados: {{ $users-> total() }} </h6>
<table class="w3-table-all">
<tr>
  <th>Usuario</th>
  <th>Rol</th>
  <th></th>
  <th></th>
</tr>

@foreach($users as $user)
<tr>
<td>{{ $user-> name}}</td>

  <td>Lectura</td>
<td>
        <a class="btn btn-primary btn-xs" class="btn btn-success btn-xs" href="{{action('AdministracionController@edit', $user->id)}}" type="submit"><span class="glyphicon glyphicon-pencil">
        </span>
      </a>
    </td>
    <td>
    <form action="{{action('AdministracionController@destroy', $user->id)}}" method="POST">
      {{csrf_field()}}
        <input name="_method" type="hidden" value="DELETE">
        <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash">
        </span>
      </button>
    </td>
</tr>
@endforeach 
</table>
{{ $users->links() }}

      </div>

<br>

      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div class="w3-container w3-padding">
        @include('chart')
        </div></div></div>
      </div>

   
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
<div class="w3-container">
            <button type="button" class="w3-button w3-block w3-theme-l4">
            <a href="{{ url('/configUsu')}}" class="w3-button-up" title="Messages">Contraseña</a></button>
          <button type="button" class="w3-button w3-block w3-theme-l4">
            <a href="{{ url('/administracion')}}" class="w3-button-up" title="Messages">Usuarios</a></button>
          <button type="button" class="w3-button w3-block w3-theme-l4"><a href="{{ url('/presentacion')}}" class="w3-button-up" title="Messages">Presentación</a></button>
          <button type="button" class="w3-button w3-block w3-theme-l4"><a href="{{ url('/blog')}}" class="w3-button-up" title="Messages">Blog</a></button>
           <button type="button" class="w3-button w3-block w3-theme-l4"><a href="{{ url('/portada')}}" class="w3-button-up" title="Messages">Portadas</a></button>
           <button type="button" class="w3-button w3-block w3-theme-l4"><a href="{{ url('/comunicacion')}}" class="w3-button-up" title="Messages">Comunicación</a></button>
            <button type="button" class="w3-button w3-block w3-theme-l4"><a href="{{ url('/noticia.all')}}" class="w3-button-up" title="Messages">Noticias y Eventos</a></button>
            <button type="button" class="w3-button w3-block w3-theme-l4"><a href="{{ url('/index') }}" class="w3-button-up" title="Messages">Arte</a></button>


             <button type="button" class="w3-button w3-block w3-theme-l4"><a href="{{ url('/programa') }}" class="w3-button-up" title="Messages">Programa de <p>formación</p></a></button>


              <button type="button" class="w3-button w3-block w3-theme-l4"><a href="{{ url('investigacion.administracion') }}" class="w3-button-up" title="Messages">Investigaciones</a></button>

              <button type="button" class="w3-button w3-block w3-theme-l4"><a href="{{ url('/administracion.encuesta') }}" class="w3-button-up" title="Messages">Encuestas</a></button>
              <button type="button" class="w3-button w3-block w3-theme-l4"><a href="{{ url('/consultas.administracion') }}" class="w3-button-up" title="Messages">Consultas</a></button>

</div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    r
  <!-- End Grid -->
  </div>
  
<!-- End Page Container -->
</div>
                </div>
            </div>
        
    </div>
</div>

@endsection
