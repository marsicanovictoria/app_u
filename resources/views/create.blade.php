

@extends('layouts.app')

@section('content')

<!-- Navbar on small screens -->

<!-- Page Container -->
<div class="w3-container w3-content" style="max-width:100%;max-width:1400px;margin-top:8%">    
  <!-- The Grid -->
  <div class="w3-row">
    <!-- Left Column -->
    <div class="w3-col m3">
      <!-- Profile -->
      <div class="w3-card w3-round w3-white">
        <div class="w3-container">
         <h4 class="w3-center">Publicaciones Recientes</h4>
        
         <hr>
         <p><i class="fa fa-pencil fa-fw w3-margin-right w3-text-theme"></i></p>
         <p><i class="fa fa-home fa-fw w3-margin-right w3-text-theme"></i> </p>
         <p><i class="fa fa-birthday-cake fa-fw w3-margin-right w3-text-theme"></i></p>
        </div>
      </div>
      <br>
      
     
      <!-- Interests --> 
      <div class="w3-card w3-round w3-white w3-hide-small">
        <div class="w3-container">
          
        </div>
      </div>
      <br>
      
      <!-- Alert Box -->
      <div class="w3-container w3-display-container w3-round w3-theme-l4 w3-border w3-theme-border w3-margin-bottom w3-hide-small">
        <span onclick="this.parentElement.style.display='none'" class="w3-button w3-theme-l3 w3-display-topright">
          <i class="fa fa-remove"></i>
        </span>
        <p><strong>Alertas</strong></p>
        <p></p>
      </div>
    
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div class="w3-col m7">
    
      <div class="w3-row-padding">
        <div class="w3-col m12">
          <div class="w3-card w3-round w3-white">
            <div class="w3-container w3-padding">
<div class="row">
    <div class="col-md-8 col-md-offset-2">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Error!</strong> Revise los campos obligatorios.<br><br>
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if(Session::has('success'))
      <div class="alert alert-info">
        {{Session::get('success')}}
      </div>
      @endif
 
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Cargar nueva configuración de presentación</h3>
        </div>
        <div class="panel-body">          
          <div class="table-container">
            <form method="POST" action=""  role="form">
              {{ csrf_field() }}
 
              <div class="form-group">
                <textarea name="pensamiento" id="pensamiento" class="form-control input-sm" placeholder="Pensamiento"></textarea>
              </div>
              <div class="form-group">
                    <textarea name="presentacion" id="presentacion" class="form-control input-sm" placeholder="Presentacion"></textarea>
              </div>
              <div class="form-group">
                  <textarea name="proposito" id="proposito" class="form-control input-sm" placeholder="Proposito"></textarea>
              </div>
              
              <div class="form-group">
                  <textarea name="proposito2" id="proposito2" class="form-control input-sm" placeholder="2do Proposito"></textarea>
              </div>
              <div class="form-group">
                  <textarea name="proposito3" id="proposito3" class="form-control input-sm" placeholder="3do Proposito"></textarea>
              </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <input type="submit"  value="Guardar" class="btn btn-success btn-block">
                  <a href="" class="btn btn-info btn-block" > Atrás</a></div>
            </form>
          </div>
        </div>
 
      </div>
    </div>
          </div>
        </div>
          </div>
        </div>
        
      </div>
      
    <!-- End Middle Column -->
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
      <div class="w3-card w3-round w3-white w3-center">
        <div class="w3-container">
          <button type="button" class="w3-button w3-block w3-theme-l4">
            <a href="{{ url('/administracion')}}" class="w3-button-up" title="Messages">Usuarios</a></button>
          <button type="button" class="w3-button w3-block w3-theme-l4"><a href="{{ url('/publicaciones')}}" class="w3-button-up" title="Messages">Publicaciones</a></button>
          <button type="button" class="w3-button w3-block w3-theme-l4" data-toggle="modal" data-target="#myModal">Comentarios</button>
          <button type="button" class="w3-button w3-block w3-theme-l4" data-toggle="modal" data-target="#myModal">Blog</button>
        </div>
      </div>
      <br>
    
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
          


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!-- End Page Container -->
</div>
<br>
 
</html> 
