		<html>
		<head>
		    <meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="{{mix('css/app.css')}}" rel="stylesheet" type="text/css">
		    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
			<!-- vinculo a bootstrap -->
<link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('css/bootstrap-theme.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Temas-->

<!-- se vincula al hoja de estilo para definir el aspecto del formulario de login-->  
<link rel="stylesheet" type="text/css" href="estilo.css">
		</head>
		<body>
		 <div class="Icon">
                    <!--Icono de usuario-->
                   
                 </div>
<div class="w3-card w3-round w3-white">
	<div id="Contenedor">
		 	<form action="" method="post" name="FormEntrar">
		 		<div class="input-group input-group-lg">
				  
				  <input type="email" class="form-control" name="correo" placeholder="Correo" id="Correo" aria-describedby="sizing-addon1" required>
				</div>
				<br>
				<div class="input-group input-group-lg">
				  
				  <input type="password" name="contra" class="form-control" placeholder="******" aria-describedby="sizing-addon1" required>
				</div>
				<br>
				      <div class="col">
        <a href="{{ route('social.auth', 'facebook') }}" class="fb btn">
          <i class="fa fa-facebook fa-fw"></i> Login with Facebook
         </a>
        <a href="#" class="twitter btn">
          <i class="fa fa-twitter fa-fw"></i> Login with Twitter
        </a>
        <a href="{{ route('social.auth', 'google') }}" class="google btn"><i class="fa fa-google fa-fw">
          </i> Login with Google+
        </a>
      </div>
				<button class="btn btn-lg btn-primary btn-block btn-signin" id="IngresoLog" type="submit"><a href="{{ url('/welcome')}}">Entrar</a></button>
				<div class="opcioncontra"><a href="">Olvidaste tu contraseña?</a></div>
		 	</form>
	
		 </div>	
		 </div>
</body>
 <!-- vinculando a libreria Jquery-->

</html>