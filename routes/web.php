<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;
Route::get('/', function () {
    return view('inicio');
});
Route::get('/home', function () {
    return view('home');
});
Route::get('/administracion', function () {
    return view('administracion');
});
Route::get('/publicaciones', function () {
    return view('publicaciones');
});
Route::get('/registro', function () {
    return view('registro');
});
Route::get('/presentacion', function () {
    return view('presentacion');
});
Route::get('/portada', function () {
    return view('portada');
});

Route::get('/blogIndex', function () {
    return view('blogIndex');
});
Route::get('/comunicacionIndex', function () {
    return view('comunicacionIndex');
});

Route::get('/foro', function () {
    return view('foro.index');
});

Route::get('/chat', function () {
    return view('chat.create');
});

Route::get('/noticia.all', function () {
    return view('noticia.all');
});

Route::get('/arte', function () {
    return view('arte.presentacion');
});


Route::get('/formacion', function () {
    return view('formacion.index');
});

Route::get('/investigacion', function () {
    return view('investigacion.index');
});

Route::get('/adminv', function () {
    return view('investigacion.administracion');
});

Route::get('/encuesta', function () {
    return view('encuesta.index');
});

Route::get('/consultas', function () {
    return view('consultas.index');
});

Route::get('/convocatoria', function () {
    return view('convocatoria');
});

Route::get('/configUsu', function () {
    return view('configuracion.administracion');
});

Auth::routes();

Route::get('auth/{provider}', 'Auth\SocialAuthController@redirectToProvider')->name('social.auth');
Route::get('auth/{provider}/callback', 'Auth\SocialAuthController@handleProviderCallback');
Auth::routes();
Route::resource('home', 'HomeController');
Route::get('/social/redirect/{provider}', 'Auth\SocialController@getSocialRedirect')->name('redirectSocialLite');
Route::get('/social/handle/{provider}', 'Auth\SocialController@getSocialHandle')->name('handleSocialLite');
Route::resource('administracion', 'AdministracionController');
Route::resource('presentacion', 'PresentacionController');
Route::resource('presComentario', 'PresComentarioController');
Route::resource('blog', 'BlogsController');
Route::resource('portada', 'PortadaController');
Route::resource('inicio', 'PortadaController');
Route::resource('blogIndex', 'BlogIndexController');
Route::post('blogIndex/store/{id}', 'BlogIndexController@store');


/*Route::get('blogIndex/show/', function(){
     
    /* Nuevo: si el argumento search está vacío regresar a la página anterior */
   
 /*   $search = urlencode(e(Input::get('titulo')));
    dd($search);
    $route = "blogIndex/show/$search";
    return redirect($route);
});*/
Route::get("blogIndex/show/", "BlogIndexController@show");
Route::get('blogIndex/pdf/{id}', 'BlogIndexController@pdf');
Route::get('blogIndex/ppt/{id}', 'BlogIndexController@ppt');
Route::get('blogIndex/docx/{id}', 'BlogIndexController@docx');
Route::get('blogIndex/xlsx/{id}', 'BlogIndexController@xlsx');
Route::get('blogIndex/video/{id}', 'BlogIndexController@video');

Route::resource('comunicacion', 'ComunicacionController');
Route::get('comunicacion/aporte/', 'ComunicacionController@aporte');

Route::resource('comunicacionIndex', 'ComunicacionIndexController');
Route::post('comunicacionIndex/store/{id}', 'ComunicacionIndexController@store');
Route::get("comunicacionIndex/show/", "ComunicacionIndexController@show");

Route::get("comunicacionIndex/content/{id}", "ComunicacionIndexController@content");

Route::resource('foro', 'ForoController');
Route::resource('comentarioForo', 'ComentarioForoController');
Route::post('comentarioForo/store/{id}', 'ComentarioForoController@store');
Route::resource('arte', 'ArteController');

Route::resource('formacion', 'FormacionController');


Route::get('/activo/',[
    'as' => 'foro.activo',
    'uses' => 'ForoController@activo'
]);

Route::get('/cerrado/',[
    'as' => 'foro.cerrado',
    'uses' => 'ForoController@cerrado'
]);
//Route::get('foro/activo/', 'ForoController@activo');

Route::resource('chat', 'ChatsController');

Route::get('/sala/',[
    'as' => 'foro.sala',
    'uses' => 'ForoController@sala'
]);

Route::post('/salachat/',[
    'as' => 'foro.store_chat',
    'uses' => 'ForoController@store_chat'
]);

Route::get('/destroy/',[
    'as' => 'comunicacionIndex.destroy',
    'uses' => 'ComunicacionIndexController@destroy'
]);

Route::get('/comunicacionIndex.administracion/',[
    'as' => 'comunicacionIndex.administracion',
    'uses' => 'ComunicacionIndexController@administracion'
]);

Route::get('/update/{id}',[
    'as' => 'foro.update',
    'uses' => 'ForoController@update'
]);

Route::resource('noticia', 'NoticiasController');

Route::get('/noticia/',[
    'as' => 'noticia.presentacion',
    'uses' => 'NoticiasController@presentacion'
]);

Route::post('/noticia/comentario/',[
    'as' => 'noticia.comentario',
    'uses' => 'NoticiasController@comentario'
]);

Route::get('/noticia/comentario/all/',[
    'as' => 'noticia.allComentario',
    'uses' => 'NoticiasController@allComentario'
]);

Route::get('/noticia/comentario/ver/{id}',[
    'as' => 'noticia.verComentario',
    'uses' => 'NoticiasController@verComentario'
]);

Route::post('/destroyComentario/{id}',[
    'as' => 'noticia.destroyComentario',
    'uses' => 'NoticiasController@destroyComentario'
]);

Route::get('/noticia.index',[
    'as' => 'noticia.index',
    'uses' => 'NoticiasController@index'
]);

Route::get('/noticia.all/',[
    'as' => 'noticia.all',
    'uses' => 'NoticiasController@all'
]);

Route::post('/update/{id}',[
    'as' => 'noticia.update',
    'uses' => 'NoticiasController@update'
]);

Route::get('/noticia.aporte/',[
    'as' => 'noticia.aporte',
    'uses' => 'NoticiasController@aporte'
]);

Route::get('/search/',[
    'as' => 'noticia.search',
    'uses' => 'NoticiasController@search'
]);

Route::get('/mis_noticias/',[
    'as' => 'noticia.mis_noticias',
    'uses' => 'NoticiasController@mis_noticias'
]);

Route::get('/edit_aporte/{id}',[
    'as' => 'noticia.edit_aporte',
    'uses' => 'NoticiasController@edit_aporte'
]);

Route::post('/updateAporte/{id}',[
    'as' => 'noticia.updateAporte',
    'uses' => 'NoticiasController@updateAporte'
]);

Route::get('noticia/pdf/{id}', 'NoticiasController@pdf');
Route::get('noticia/ppt/{id}', 'NoticiasController@ppt');
Route::get('noticia/word/{id}', 'NoticiasController@word');
Route::get('noticia/excel/{id}', 'NoticiasController@excel');
Route::get('noticia/video/{id}', 'NoticiasController@video');

Route::get('/store/{id}',[
    'as' => 'administracion.store',
    'uses' => 'AdministracionController@store'
]);

Route::get('/refreshCaptcha/',[
    'as' => 'noticia.refreshCaptcha',
    'uses' => 'NoticiasController@refreshCaptcha'
]);

Route::get('/arte/',[
    'as' => 'arte.presentacion',
    'uses' => 'ArteController@presentacion'
]);


Route::get('/index',[
    'as' => 'arte.index',
    'uses' => 'ArteController@index'
]);


Route::post('/updateArte/{id}',[
    'as' => 'arte.updateArte',
    'uses' => 'ArteController@updateArte'
]);

Route::get('/misAportes',[
    'as' => 'arte.misAportes',
    'uses' => 'ArteController@misAportes'
]);

Route::get('/ShowMusica',[
    'as' => 'arte.ShowMusica',
    'uses' => 'ArteController@ShowMusica'
]);

Route::get('/ShowDanza',[
    'as' => 'arte.ShowDanza',
    'uses' => 'ArteController@ShowDanza'
]);

Route::get('/ShowPintura',[
    'as' => 'arte.ShowPintura',
    'uses' => 'ArteController@ShowPintura'
]);

Route::get('/ShowOtros',[
    'as' => 'arte.ShowOtros',
    'uses' => 'ArteController@ShowOtros'
]);

Route::get('/ShowChistes',[
    'as' => 'arte.chistes',
    'uses' => 'ArteController@ShowChistes'
]);

Route::get('/pdf/{id}',[
    'as' => 'arte.pdf',
    'uses' => 'ArteController@pdf'
]);

Route::get('/all/',[
    'as' => 'formacion.all',
    'uses' => 'FormacionController@all'
]);

Route::get('/programa/',[
    'as' => 'formacion.programa',
    'uses' => 'FormacionController@administracion'
]);

Route::get('/formacion.aporte/',[
    'as' => 'formacion.aporte',
    'uses' => 'FormacionController@aporte'
]); 

Route::get('/formacion.area/',[
    'as' => 'formacion.area',
    'uses' => 'FormacionController@area'
]); 

Route::get('formacion/pdf/{id}', 'FormacionController@pdf');
Route::get('formacion/ppt/{id}', 'FormacionController@ppt');
Route::get('formacion/word/{id}', 'FormacionController@word');
Route::get('formacion/excel/{id}', 'FormacionController@excel');
Route::get('formacion/video/{id}', 'FormacionController@video');
Route::post('formacion/comentario/{id}', 'FormacionController@comentario');
Route::post('formacion/update/{id}', 'FormacionController@update');

Route::get('/persona/',[
    'as' => 'persona',
    'uses' => 'FormacionController@persona'
]); 
Route::get('/contexto/',[
    'as' => 'contexto',
    'uses' => 'FormacionController@contexto'
]); 
Route::get('/conocimiento/',[
    'as' => 'conocimiento',
    'uses' => 'FormacionController@conocimiento'
]); 
Route::get('/tecnologia/',[
    'as' => 'tecnologia',
    'uses' => 'FormacionController@tecnologia'
]); 
Route::get('/empresa/',[
    'as' => 'empresa',
    'uses' => 'FormacionController@empresa'
]); 

Route::resource('investigacion', 'InvestigacionsController');

Route::get('/show/',[
    'as' => 'investigacion.show',
    'uses' => 'InvestigacionsController@show'
]);

Route::get('/aporte/',[
    'as' => 'investigacion.aporte',
    'uses' => 'InvestigacionsController@aporte'
]);

Route::get('investigacion/edit_aporte/{id}', 'InvestigacionsController@edit_aporte');
//Route::post('investigacion/detail/{id}', 'InvestigacionsController@detail');


//Route::post('investigacion/update_aporte/{id}', 'InvestigacionsController@update_aporte');
Route::post('/update_aporte/{id}',[
    'as' => 'investigacion.update_aporte',
    'uses' => 'InvestigacionsController@update_aporte'
]);

Route::get('/investigacion.administracion/',[
    'as' => 'investigacion.administracion',
    'uses' => 'InvestigacionsController@administracion'
]);


Route::get('/content/{id}',[
    'as' => 'investigacion.content',
    'uses' => 'InvestigacionsController@content'
]);

Route::post('/comentario/{id}',[
    'as' => 'investigacion.comentario',
    'uses' => 'InvestigacionsController@comentario'
]);

Route::post('/eliminarComentario/{id}',[
    'as' => 'comunicacion.eliminarComentario',
    'uses' => 'ComunicacionIndexController@eliminarComentario'
]);

Route::get('/comunicacion.verComentario/{id}',[
    'as' => 'comunicacion.verComentario',
    'uses' => 'ComunicacionIndexController@verComentario'
]);

Route::post('/comunicacion.comentario/',[
    'as' => 'comunicacion.comentario',
    'uses' => 'ComunicacionController@comentario'
]);

Route::get('/comunicacion.administracion/',[
    'as' => 'comunicacion.administracion',
    'uses' => 'ComunicacionIndexController@administracion'
]);

Route::get('/comunicacion.nuevaComunicacion/',[
    'as' => 'comunicacion.nuevaComunicacion',
    'uses' => 'ComunicacionController@nuevaComunicacion'
]);


Route::get('/nueva/',[
    'as' => 'formacion.nueva',
    'uses' => 'FormacionController@nueva'
    ]);

Route::get('/verPdf/{id}',[
    'as' => 'verPdf',
    'uses' => 'InvestigacionsController@verPdf'
    ]);

Route::get('/verComentario/{id}',[
    'as' => 'investigacion.verComentario',
    'uses' => 'InvestigacionsController@verComentario'
]);

Route::get('/configuracion',[
    'as' => 'investigacion.configuracion',
    'uses' => 'InvestigacionsController@configuracion'
]);

Route::post('/storeConf',[
    'as' => 'investigacion.storeConf',
    'uses' => 'InvestigacionsController@storeConf'
]);

Route::get('/verSPSS/{id}',[
    'as' => 'verSPSS',
    'uses' => 'InvestigacionsController@verSPSS'
    ]);

Route::get('/verEcxel/{id}',[
    'as' => 'verEcxel',
    'uses' => 'InvestigacionsController@verEcxel'
    ]);

Route::get('/word/{id}',[
    'as' => 'arte.word',
    'uses' => 'ArteController@word'
]);

Route::get('/excel/{id}',[
    'as' => 'arte.excel',
    'uses' => 'ArteController@excel'
]);

Route::get('/video/{id}',[
    'as' => 'arte.video',
    'uses' => 'ArteController@video'
]);

Route::get('/ppt/{id}',[
    'as' => 'arte.ppt',
    'uses' => 'ArteController@ppt'
]);

/*Route::resource('encuesta', 'EncuestaController');

Route::get('/administracion.encuesta',[
    'as' => 'administracion.encuesta',
    'uses' => 'EncuestaController@administracion'
]);*/

/*Route::get('/encuesta', function () {
    return view('survey.home');
});*/

Route::get('/encuesta', 'SurveyController@home')->name('survey.home');
 
Route::get('/survey/new', 'SurveyController@new_survey')->name('new.survey');
Route::get('/survey/{survey}', 'SurveyController@detail_survey')->name('detail.survey');
Route::get('/survey/view/{survey}', 'SurveyController@view_survey')->name('view.survey');
Route::get('/survey/answers/{survey}', 'SurveyController@view_survey_answers')->name('view.survey.answers');
Route::get('/survey/{survey}/delete', 'SurveyController@delete_survey')->name('delete.survey');
 
Route::get('/survey/{survey}/edit', 'SurveyController@edit')->name('edit.survey');
Route::patch('/survey/{survey}/update', 'SurveyController@update')->name('update.survey');
 
Route::post('/survey/view/{survey}/completed', 'AnswerController@store')->name('complete.survey');
Route::post('/survey/create', 'SurveyController@create')->name('create.survey');
 
// Questions related
Route::post('/survey/{survey}/questions', 'QuestionController@store')->name('store.question');
 
Route::get('/question/{question}/edit', 'QuestionController@edit')->name('edit.question');
Route::patch('/question/{question}/update', 'QuestionController@update')->name('update.question');

Route::get('/comunicacion/comentarios',[
    'as' => 'comunicacion.comentarios',
    'uses' => 'ComunicacionController@showComentarios'
]);

Route::resource('consultas', 'ConsultasController');

Route::get('/consultas.misConsultas',[
    'as' => 'consultas.misConsultas',
    'uses' => 'ConsultasController@misConsultas'
]);

Route::post('/consultas/guardarComentario/{id}',[
    'as' => 'consultas.guardarComentario',
    'uses' => 'ConsultasController@guardarComentario'
]);

Route::get('/consultas.administracion',[
    'as' => 'consultas.administracion',
    'uses' => 'ConsultasController@administracion'
]);

Route::post('/consultas.update/{id}',[
    'as' => 'consultas.update',
    'uses' => 'ConsultasController@update'
]);

Route::get('/comunicaciones.presentacion',[
    'as' => 'comunicaciones.presentacion',
    'uses' => 'ComunicacionController@presentacion'
]);

Route::post('/comunicaciones.updatePresentacion/{id}',[
    'as' => 'comunicaciones.updatePresentacion',
    'uses' => 'ComunicacionController@updatePresentacion'
]);

Route::get('/blog.presentacion',[
    'as' => 'blog.presentacion',
    'uses' => 'BlogsController@presentacion'
]);

Route::post('/blog.updatePresentacion/{id}',[
    'as' => 'blog.updatePresentacion',
    'uses' => 'BlogsController@updatePresentacion'
]);

Route::get('/formacion.presentacion',[
    'as' => 'formacion.presentacion',
    'uses' => 'FormacionController@presentacion'
]);

Route::post('/formacion.updatePresentacion/{id}',[
    'as' => 'formacion.updatePresentacion',
    'uses' => 'FormacionController@updatePresentacion'
]);

Route::get('/consultas.presentacion',[
    'as' => 'consultas.presentacion',
    'uses' => 'ConsultasController@presentacion'
]);

Route::post('/consultas.updatePresentacion/{id}',[
    'as' => 'consultas.updatePresentacion',
    'uses' => 'ConsultasController@updatePresentacion'
]);

Route::get('/arte.congifpresentacion',[
    'as' => 'arte.congifpresentacion',
    'uses' => 'ArteController@congifpresentacion'
]);

Route::post('/arte.updatePresentacion/{id}',[
    'as' => 'arte.updatePresentacion',
    'uses' => 'ArteController@updatePresentacion'
]);

Route::get('/noticia.configpresentacion',[
    'as' => 'noticia.configpresentacion',
    'uses' => 'NoticiasController@configpresentacion'
]);

Route::post('/noticia.updatePresentacion/{id}',[
    'as' => 'noticia.updatePresentacion',
    'uses' => 'NoticiasController@updatePresentacion'
]);

Route::get('/investigacion.configpresentacion',[
    'as' => 'investigacion.configpresentacion',
    'uses' => 'InvestigacionsController@configpresentacion'
]);

Route::post('/investigacion.updatePresentacion/{id}',[
    'as' => 'investigacion.updatePresentacion',
    'uses' => 'InvestigacionsController@updatePresentacion'
]);

Route::post('/postReset/{id}',[
    'as' => 'postReset',
    'uses' => 'AdministracionController@postReset'
]);



$this->get('password/change', 'Auth\ChangePasswordController@showChangePasswordForm')->name('password.change');
        $this->post('password/change', 'Auth\ChangePasswordController@change')->name('password.change.post');
//Route::auth();