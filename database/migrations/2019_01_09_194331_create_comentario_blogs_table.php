<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComentarioBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentario_blogs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('comentario');
            $table->string('nombre');
            $table->string('email');
            $table->integer('id_blog');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comentario_blogs');
    }
}
