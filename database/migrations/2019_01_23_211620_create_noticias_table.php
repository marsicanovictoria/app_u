<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('noticias', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('titulo');
            $table->string('contenido');
            $table->string('carrera');
            $table->string('entidad');
            $table->string('imagen');
            $table->string('id_usuario');
            $table->string('pdf');
            $table->string('word');
            $table->string('excel');
            $table->string('ppt');
            $table->string('video');
            $table->string('tipo_identificacion');
            $table->integer('numero_identificacion');
            $table->string('nombre');
            $table->integer('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('noticias');
    }
}
