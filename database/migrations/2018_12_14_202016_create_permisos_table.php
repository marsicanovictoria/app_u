<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermisosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permisos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->boolean('activarusuario');
            $table->boolean('desactivarusuario');
            $table->boolean('lecturapresentacion');
            $table->boolean('escriturapresentacion');
            $table->boolean('lecturanoticias');
            $table->boolean('escrituranoticias');
            $table->boolean('lecturaformacionpersonal');
            $table->boolean('escrituraformacionpersonal');
            $table->boolean('lecturainvestigaciones');
            $table->boolean('escriturainvestigaciones');
            $table->boolean('lecturacomunicaciones');
            $table->boolean('escrituracomunicaciones');
            $table->boolean('lecturablog');
            $table->boolean('escriturablog');
            $table->boolean('lecturaconsulta');
            $table->boolean('escrituraconsulta');
            $table->boolean('lecturatesislibro');
            $table->boolean('escrituratesislibro');
            $table->boolean('lecturaamenidades');
            $table->boolean('escrituraamenidades');
            $table->boolean('lecturaencuesta');
            $table->boolean('escrituraencuesta');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permisos');
    }
}
