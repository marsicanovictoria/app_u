<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portadas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('proposito');
            $table->string('estructura');
            $table->string('for_personal');
            $table->string('arte');
            $table->string('comunicaciones');
            $table->string('noticias');
            $table->string('investigaciones');
            $table->string('estudios');
            $table->string('tesis');
            $table->string('cursos');
            $table->string('encuestas');
            $table->string('imagen');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portadas');
    }
}
