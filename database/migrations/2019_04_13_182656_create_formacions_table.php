<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
                Schema::create('formacions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('area');
            $table->string('convocatoria');
            $table->string('requisito');
            $table->string('lugar');
            $table->string('responsable');
            $table->string('pdf');
            $table->string('doc');
            $table->string('excel');
            $table->string('ppt');
            $table->string('video');
            $table->string('imagen');
            $table->timestamps('fecha_ini');
            $table->timestamps('fecha_fin');
            $table->integer('id_usuario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formacions');
    }
}
