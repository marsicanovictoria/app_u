<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foros', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('eje');
            $table->string('contenido');
            $table->integer('activo');
            $table->integer('id_usuario');
            $table->string('titulo');
            $table->string('mensaje');
            $table->integer('visto');
            $table->integer('foro');
            $table->timestamps('fecha_fin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foros');
    }
}
