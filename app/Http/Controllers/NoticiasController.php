<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Noticias;
use DB;
use Input;
use Image; 
use MimeTypeGuesser;
use Response;
use App\Identificacion;
use App\ComentarioNoticia;
use App\Contenidos;

class NoticiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $noticias= Noticias::orderBy('id', 'DESC')->get();
        $c= ComentarioNoticia::orderBy('id', 'DESC')->get();
        /*$noticias = DB::table('noticias')
        ->join ('users', 'noticias.id_usuario', '=', 'users.id')
        ->select('noticias.*', 'users.name', 'users.email')
        ->orderBy('noticias.id', 'desc')
        ->get();*/

        return view('noticias.index')
        ->with('noticias', $noticias)
        ->with('c',$c);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $identificacion = Identificacion::all('id', 'tipo_identificacion')->pluck('tipo_identificacion', 'tipo_identificacion');
        return \View::make('noticias.create', compact('identificacion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $noticia = new Noticias();
        $noticia->titulo = $request->titulo;
        $noticia->contenido = $request->contenido;
        $noticia->carrera = $request->carrera;
        $noticia->entidad = $request->entidad;
        $noticia->id_usuario = auth()->id();
        $noticia->tipo_identificacion = $request->identificacion;
        $noticia->numero_identificacion = $request->numero_identificacion;
        $noticia->nombre = $request->nombre;
        $noticia->email = $request->email;

        $request->validate([

            'captcha' => 'required|captcha'
        ]);


        
        if (($request->noticiaImg) != NULL)
            {
        $noticia->imagen =$request->file('noticiaImg')->store('public');
    }if (($request->pdf) != NULL) {
        
                $noticiapdf =$request->file('pdf')->store('public/store');
        $noticia->pdf = $noticiapdf;
    }if (($request->ppt) != NULL) {
        
        $noticiappt =$request->file('ppt')->store('public/store');
        $noticia->ppt = $noticiappt;
    }if (($request->word) != NULL) {
        
        $noticiaword =$request->file('word')->store('public/store');
        $noticia->word = $noticiaword;
    }if (($request->excel) != NULL) {
        $noticiaexcel =$request->file('excel')->store('public/store');
        $noticia->excel = $noticiaexcel;
    }if (($request->video) != NULL) {
        $noticiavideo =$request->file('video')->store('public/store');
        $noticia->video = $noticiavideo;
    }
    //dd($noticia);

        if($noticia->save()){
            return redirect()->route('noticia.presentacion')->with('success-message', 'File has been uploaded');
        }else{
            return redirect('uploads')->with('error-message', 'An error ocurred saving data into database');
        }       

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $noticias = Noticias::find($id);
        $id = 5;
       $contenido=Contenidos::find($id);  
        return view('noticiaShow')->with('noticias', $noticias)
        ->with('contenido', $contenido);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $noticias= Noticias::find($id);
        $identificacion = Identificacion::all('id', 'tipo_identificacion')->pluck('tipo_identificacion', 'tipo_identificacion');

        return view('noticias.edit')->with('noticias', $noticias)
        ->with('identificacion', $identificacion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $this->validate($request,[ 'titulo'=>'required', 'contenido'=>'required', 'carrera'=>'required', 'entidad'=>'required', 'nombre'=>'required','email'=>'required', 'identificacion'=>'required', 'numero_identificacion'=>'required']);

       Noticias::find($id)->update($request->all());


        return redirect()->route('noticia.all')->with('success','Registro actualizado satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            Noticias::find($id)->delete();
        return redirect()->route('noticia.all')->with('success','Registro eliminado satisfactoriamente');
    }

        public function presentacion()
    {
                        $id = 7;
       $contenido=Contenidos::find($id);  
        return view('noticias')
        ->with('contenido',$contenido);
    }

        public function all()
    {
        $noticias = Noticias::orderBy('id', 'DESC')->paginate(15);
        return view('noticias.all')->with('noticias',$noticias);
    }

    public function aporte()
    {
    $noticias = Noticias::orderBy('id', 'DESC')
    ->where('id_usuario','=', auth()->id())
    ->paginate(15);
               return view('noticias.aporte')
               ->with('noticias', $noticias); 
    }

    public function search(Request $request)
    {
        $titulo = $request->titulo;
        $noticias=Noticias::where('titulo', 'LIKE', "%{$titulo}%" )->get();

        return  view('noticiaIndex', compact('noticias'));
    }

    public function mis_noticias(){
            $noticias = Noticias::orderBy('id', 'DESC')
            ->where('id_usuario','=', auth()->id())->paginate(15);
               return view('noticiaAporte')
               ->with('noticias', $noticias); 
    }

    public function edit_aporte($id){
                $identificacion = Identificacion::all('id', 'tipo_identificacion')->pluck('tipo_identificacion', 'tipo_identificacion');
            $noticias = Noticias::find($id);
               return view('editAporte')
               ->with('noticias', $noticias)
               ->with('identificacion', $identificacion); 
    }

        public function updateAporte(Request $request, $id)
    {
            $this->validate($request,[ 'titulo'=>'required', 'contenido'=>'required', 'carrera'=>'required', 'entidad'=>'required', 'nombre'=>'required','email'=>'required', 'identificacion'=>'required', 'numero_identificacion'=>'required']);

       Noticias::find($id)->update($request->all());


        return redirect()->route('noticias.index')->with('success','Registro actualizado satisfactoriamente');
    }

    public function pdf($id){
        $noticias=Noticias::find($id);
        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $file= $path.$noticias->pdf;
        $headers = array(
              'Content-Type: application/pdf',
            );

    return Response::download($file, 'archivo.pdf', $headers);
    }

        public function ppt($id){
        $noticias=Noticias::find($id);
        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $file= $path.$noticias->ppt;
        
        $headers = array(
              'Content-Type: application/ppt',
            );

    return Response::download($file, 'archivo.ppt', $headers);
    }

    public function word($id){
        $noticias=Noticias::find($id);
        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $file= $path.$noticias->word;
        $headers = array(
              'Content-Type: application/docx',
            );

    return Response::download($file, 'archivo.docx', $headers);
    }

    public function excel($id){
        $noticias=Noticias::find($id);
        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $file= $path.$noticias->excel;
        $headers = array(
              'Content-Type: application/xlsx',
            );

    return Response::download($file, 'archivo.xlsx', $headers);
    }

    public function video($id){
        $noticias=Noticias::find($id);
        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $file= $path.$noticias->video;
        $headers = array(
              'Content-Type: application/mp4',
            );

    return Response::download($file, 'archivo.mp4', $headers);
    }

        public function refreshCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }

   /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function comentario(Request $request)
    {
        $c = new ComentarioNoticia();
        $c->nombre = $request->nombre;
        $c->apellido = $request->apellido;
        $c->email = $request->email;
        $c->comentario = $request->comentario;

       if($c->save()){
            return redirect()->route('noticia.index')->with('success-message', 'File has been uploaded');
        }else{
            return redirect('uploads')->with('error-message', 'An error ocurred saving data into database');
        } 
    }

        /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verComentario($id)
    {
    $com= ComentarioNoticia::find($id);
        return view('noticias.detalleComentario')->with('com',$com);
    }

        public function allComentario()
    {
        $comen = ComentarioNoticia::orderBy('id', 'DESC')->paginate(15);
        return view('noticias.comentario')->with('comen',$comen);
    }

        public function destroyComentario($id)
    {
            ComentarioNoticia::find($id)->delete();
        return redirect()->route('noticia.allComentario')->with('success','Registro eliminado satisfactoriamente');
    }    

        public function configpresentacion()
    {
        $id = 7;
       $contenido=Contenidos::find($id);        
        return view('noticias.config')->with ('contenido', $contenido);
    }

    public function updatePresentacion(Request $request, $id)
    {
        $this->validate($request,[ 'contenido'=>'required']);

       Contenidos::find($id)->update($request->all());


        return redirect()->route('noticia.all')->with('success','Registro actualizado satisfactoriamente');
    }
}
