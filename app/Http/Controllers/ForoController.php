<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Foro;
use App\Ejes;
use DB;
use App\ComentarioForo;

class ForoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $foro = DB::table('foros')
        ->join ('users', 'foros.id_usuario', '=', 'users.id')
        ->select('foros.*', 'users.name')
        ->orderBy('foros.id', 'desc')
        ->get();

    return view('foro.index', compact('foro'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $eje = Ejes::all('id', 'descripcion')->pluck('descripcion', 'descripcion');
        return \View::make('foro.create', compact('eje'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $foro = new Foro();
        $foro->titulo = $request->titulo;
        $foro->contenido = $request->contenido;
        $foro->eje = $request->eje;
        $foro->id_usuario = auth()->id();
        $foro->activo = 1;
        $foro->foro = $request->optradio;
        $foro->created_at = $request->fecha1;
        $foro->fecha_fin = $request->fecha2;

        $success = $foro->save() ? $request->session()->flash('success', '¡Registro exitoso!') : $request->session()->flash('success', 'Ooops! Algo salio mal :(');

        $foro = Foro::orderBy('id', 'DESC')->paginate(15);
        return view('foro.index')->with ('foro', $foro);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $foro=Foro::find($id);

        $forocomen = DB::table('comentario_foros')
        ->join ('users', 'comentario_foros.id_usuario', '=', 'users.id')
        ->select('comentario_foros.*', 'users.name')
        ->where('comentario_foros.id_foro','=',$id)
        ->get();

        return view('foro.content') 
        ->with('forocomen', $forocomen)
        ->with('foro', $foro);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {

       $foro= Foro::find($id);
       $foro->activo = 0;

        if($foro->save()){
                return redirect()->route('foro.index')->with('success-message', 'File has been uploaded');
        }else{
            return redirect('uploads')->with('error-message', 'An error ocurred saving data into database');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function activo()
    {
        $foro = DB::table('foros')
        ->join ('users', 'foros.id_usuario', '=', 'users.id')
        ->select('foros.*', 'users.name')
        ->where('foros.activo', '=', '1' )
        ->get();

    return view('foro.index', compact('foro', $foro));

    }

        public function cerrado()
    {
        $foro = DB::table('foros')
        ->join ('users', 'foros.id_usuario', '=', 'users.id')
        ->select('foros.*', 'users.name')
        ->where('foros.activo', '=', '0' )
        ->get();

    return view('foro.index', compact('foro', $foro));

    }

    public function sala()
    {

    return view('chat.sala'); 
    }
}
