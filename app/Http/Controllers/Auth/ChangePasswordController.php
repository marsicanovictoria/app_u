<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChangePasswordController extends Controller
{

	    public function index()  
   {  
     return view('configuracion.administracion');  
   }  
    public function __construct()  
   {  
     $this->middleware('auth');  
   }  
   
   //show form:  
   public function showChangePasswordForm(Request $request, User $user)  
   {  
     return view('auth.passwords.change')->with(  
       ['user' => $user]  
     );  
   }  
   
   public function change(Request $request)  
   {  
     //Get current user data:  
     //$user = auth()->user();  
   
     //Validation:  
     $this->validate($request, [  
       'password'     => 'required|min:6|confirmed',  
       'current_password' => 'isCurrentPassword' ,  
     ]);  
   
     $request->user()->fill([  
       'password' => Hash::make($request->password)  
     ])->save();  
   
     return redirect('home')->with('info','Your password has been updated succesfully.');  
   }  
   public function boot(){
   	                Validator::extend('isCurrentPassword',function ($attribute, $value, $parameters, $validator){
            $user = auth()->user();
            if(Hash::check($value, $user->password)){
                return true;
            }
            return false;
        });
   }
}
