<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ComentarioForo;
use App\Foro;
use DB;

class ComentarioForoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $foro = new ComentarioForo();
        $foro->comentario = $request->comentario;
        $foro->id_foro = $request->id;
        $foro->id_usuario = auth()->id();

        $success = $foro->save() ? $request->session()->flash('success', '¡Registro exitoso!') : $request->session()->flash('success', 'Ooops! Algo salio mal :(');

        $foro=Foro::find($id);

        $forocomen = DB::table('comentario_foros')
        ->join ('users', 'comentario_foros.id_usuario', '=', 'users.id')
        ->select('comentario_foros.*', 'users.name')
        ->where('comentario_foros.id_foro','=',$id)
        ->get();

        return view('foro.content') 
        ->with('forocomen', $forocomen)
        ->with('foro', $foro);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
