<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PresComentario;
use App\Presentacion;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $presentacion=Presentacion::orderBy('id')->paginate(3);
        return view('home')
        ->with('presentacion', $presentacion);
    }

    public function store(Request $request)
    {
        //
        $this->validate($request,[ 'comentarios'=>'required', 'nombre'=>'required', 'email'=>'required']);
 
        PresComentario::create($request->all());

        return redirect()->route('home.index')->with('success','Registro creado satisfactoriamente');
    }
}
