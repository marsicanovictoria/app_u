<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Investigacions;
use App\ConfiguracionInv;
use App\Cominvs;
use App\Contenidos;

class InvestigacionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
              $id = 6;
       $contenido=Contenidos::find($id);  
        return view('investigacion.index')->with('contenido',$contenido);;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('investigacion.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $i = new Investigacions();
        $i->titulo = $request->titulo;
        $i->descripcion = $request->descripcion;
        $i->documento = $request->documento;
        $i->id_usuario = auth()->id();
        //'g-recaptcha-response' = 'required|captcha'

        if (($request->invpdf) != NULL) {
                $invpdf =$request->file('invpdf')->store('public/store');
        $i->invpdf = $invpdf;
    }elseif (($request->invexcel) != NULL) {
                $invexcel =$request->file('invexcel')->store('public/store');
        $i->invexcel = $invexcel;
    }elseif (($request->invsps) != NULL) {
                $invsps =$request->file('invsps')->store('public/store');
        $i->invsps = $invsps;
    }

        if($i->save()){
            return redirect()->route('investigacion.index')->with('success-message', 'File has been uploaded');
        }else{
            return redirect('uploads')->with('error-message', 'An error ocurred saving data into database');
        }  

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $inv = Investigacions::orderBy('id', 'DESC')->paginate(15);
        return view('investigacion.all')->with('inv',$inv);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $inv = Investigacions::find($id);
        return view('investigacion.editar')->with('inv',$inv);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
                $this->validate($request,[ 'titulo'=>'required', 'descripcion'=>'required']);

       Investigacions::find($id)->update($request->all());


        return redirect()->route('investigacion.administracion')->with('success','Registro actualizado satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Investigacions::find($id)->delete();
        $inv = Investigacions::orderBy('id', 'DESC')
        ->paginate(15);
        return view('investigacion.administracion')->with('inv',$inv);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function content($id)
    {
        $inv = Investigacions::find($id);
        $c = Cominvs::orderBy('id', 'DESC')
        ->where('id_inv','=', $id);
        return view('investigacion.detail')->with('inv',$inv)
        ->with('c',$c);
    }
        /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function aporte()
    {
        $inv = Investigacions::orderBy('id', 'DESC')
        ->where('id_usuario','=', auth()->id())
        ->paginate(15);
        return view('investigacion.aporte')->with('inv',$inv);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_aporte($id)
    {
        $inv = Investigacions::find($id);
        return view('investigacion.edit')->with('inv',$inv);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_aporte(Request $request, $id)
    {
                $this->validate($request,[ 'titulo'=>'required', 'descripcion'=>'required']);

       Investigacions::find($id)->update($request->all());


        return redirect()->route('investigacion.aporte')->with('success','Registro actualizado satisfactoriamente');
    }

        /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function administracion()
    {
        $inv = Investigacions::orderBy('id', 'DESC')
        ->paginate(15);
        return view('investigacion.administracion')->with('inv',$inv);
    }


   /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function comentario(Request $request, $id)
    {
        $c = new Cominvs();
        $c->nombre = $request->nombre;
        $c->apellido = $request->apellido;
        $c->email = $request->email;
        $c->comentario = $request->comentario;
        $c->id_inv = $id;
    

        if($c->save()){
            return redirect()->route('investigacion.index')->with('success-message', 'File has been uploaded');
        }else{
            return redirect('uploads')->with('error-message', 'An error ocurred saving data into database');
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verPdf($id)
    {
        $inv = Investigacions::find($id);

        $ruta= $inv->invpdf;

        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $ruta= $inv->invpdf;
        $file= $path.$ruta;
        // dd($file);
          $html = '<html><body>'
             . '<p>Blog: '.$file.'</p>'
             . '</body></html>';
 

    return response()->file($file);

    // return response()->file($file);

     //return InvestigacionsController::load($html, 'A4', 'portrait')->show();
    }

        /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verComentario($id_inv)
    {
    $inv = Cominvs::orderBy('id_inv', 'DESC')
        ->paginate(15);
        return view('investigacion.comentario')->with('inv',$inv);
    }

    public function destroyComentario($id)
    {
        Cominvs::find($id)->delete();
        $inv = Cominvs::orderBy('id', 'DESC')
        ->paginate(15);
        return view('investigacion.comentario')->with('inv',$inv);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function configuracion()
    {
        $inv = ConfiguracionInv::orderBy('id');
        return view('investigacion.configuracion')
                ->with('inv',$inv);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function storeConf(Request $request, $id)
    {
                $this->validate($request,[ 'presentacion'=>'required']);

       ConfiguracionInv::find($id)->update($request->all());


        return redirect()->route('investigacion.administracion')->with('success','Registro actualizado satisfactoriamente');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verSPSS($id)
    {
        $inv = Investigacions::find($id);

        $ruta= $inv->invsps;

        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $ruta= $inv->invsps;
        $file= $path.$ruta;
        // dd($file);
          $html = '<html><body>'
             . '<p>Blog: '.$file.'</p>'
             . '</body></html>';
 

    return response()->file($file);

    // return response()->file($file);

     //return InvestigacionsController::load($html, 'A4', 'portrait')->show();
    }
 /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verEcxel($id)
    {
        $inv = Investigacions::find($id);

        $ruta= $inv->invexcel;

        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $ruta= $inv->invexcel;
        $file= $path.$ruta;
        // dd($file);
          $html = '<html><body>'
             . '<p>Blog: '.$file.'</p>'
             . '</body></html>';
 

    return response()->file($file);

    // return response()->file($file);

     //return InvestigacionsController::load($html, 'A4', 'portrait')->show();
    }

        public function configpresentacion()
    {
        $id = 5;
       $contenido=Contenidos::find($id);        
        return view('investigacion.presentacion')->with ('contenido', $contenido);
    }

    public function updatePresentacion(Request $request, $id)
    {
        $this->validate($request,[ 'contenido'=>'required']);

       Contenidos::find($id)->update($request->all());


        return redirect()->route('investigacion.administracion')->with('success','Registro actualizado satisfactoriamente');
    }

}
