<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comunicacion;
use App\Ejes;
use App\ComenComunicacion;
use App\Contenidos;

class ComunicacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    $comunicacion = Comunicacion::orderBy('id', 'DESC')->paginate(15);
    return view('comunicacion.index')->with ('comunicacion', $comunicacion);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $eje = Ejes::all('id', 'descripcion')->pluck('descripcion', 'descripcion');
        return \View::make('comunicacion.crear', compact('eje'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comunicacion = new Comunicacion();
        $comunicacion->titulo = $request->titulo;
        $comunicacion->comunicado = $request->comunicacion;
        $comunicacion->eje = $request->eje;
        $comunicacion->id_usuario = auth()->id();
                $request->validate([

            'captcha' => 'required|captcha'
        ]);

        $success = $comunicacion->save() ? $request->session()->flash('success', '¡Registro exitoso!') : $request->session()->flash('success', 'Ooops! Algo salio mal :(');

        $comunicacion = Comunicacion::orderBy('id', 'DESC')->paginate(15);
        return view('comunicacionIndex')->with ('comunicacion', $comunicacion);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
               $comunicacion = Comunicacion::where('id_usuario','=', auth()->id())->paginate(10);
               return view('comunicacion.aporte')
               ->with('comunicacion', $comunicacion);   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comunicacion=Comunicacion::find($id);
        $eje = Ejes::all('id', 'descripcion')->pluck('descripcion', 'descripcion');
        
        return view('comunicacion.editar') 
        ->with('comunicacion', $comunicacion)
        ->with('eje', $eje);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[ 'titulo'=>'required', 'comunicado'=>'required', 'eje'=>'required']);

       Comunicacion::find($id)->update($request->all());


        return redirect()->route('comunicacion.index')->with('success','Registro actualizado satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Comunicacion::find($id)->delete();
        return redirect()->route('comunicacion.index')->with('success','Registro eliminado satisfactoriamente');
    }

        public function aporte()
    {
 
        
        return view('comunicacion.show'); 
        
    }

    public function comentario(Request $request)
    {
        $c = new ComenComunicacion();
        $c->nombre = $request->nombre;
        $c->apellido = $request->apellido;
        $c->email = $request->email;
        $c->comentario = $request->comentario;

       if($c->save()){
            return redirect()->route('comunicacion.index')->with('success-message', 'File has been uploaded');
        }else{
            return redirect('uploads')->with('error-message', 'An error ocurred saving data into database');
        } 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showComentarios()
    {
    $comen = ComenComunicacion::orderBy('id', 'DESC')->paginate(15);
    return view('comunicacion.comentarios')->with ('comen', $comen);
    }

        public function nuevaComunicacion()
    {
        $eje = Ejes::all('id', 'descripcion')->pluck('descripcion', 'descripcion');
        return \View::make('comunicacion.nuevaComunicacion', compact('eje'));
    }

        public function presentacion()
    {
        $id = 1;
       $comunicacion=Contenidos::find($id);        
        return view('comunicacion.presentacion')->with ('comunicacion', $comunicacion);
    }

    public function updatePresentacion(Request $request, $id)
    {
        $this->validate($request,[ 'contenido'=>'required']);

       Contenidos::find($id)->update($request->all());


        return redirect()->route('comunicacion.index')->with('success','Registro actualizado satisfactoriamente');
    }
}
