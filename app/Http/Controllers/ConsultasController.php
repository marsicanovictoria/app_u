<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consultas;
use App\Comconsultas;
use App\Contenidos;

class ConsultasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $id = 4;
       $contenido=Contenidos::find($id); 
        return view('consultas.index')
        ->with('contenido', $contenido); ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $consulta = new Consultas();
        $consulta->nombre = $request->nombre;
        $consulta->pseudonimo = $request->pseudonimo;
        $consulta->email = $request->email;
        $consulta->telefono = $request->telefono;
        $consulta->consulta = $request->pseudonimo;
        $consulta->id_usu_con = auth()->id();
        $consulta->activo = 1;

        if($consulta->save()){
            return redirect()->route('consultas.index')->with('success-message', 'Consulta enviada con éxito.');
        }else{
            return redirect('uploads')->with('error-message', 'A ocurrido un error al enviar el formulario.');
        }  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $consultas= Consultas::find($id);
                $comentarios= Comconsultas::find($id);
        return view('consultas.content')->with('consultas', $consultas)
        ->with('comentarios', $comentarios);
    }
        public function misConsultas()
    {
        $consultas = Consultas::orderBy('id', 'DESC')
            ->where('id_usu_con','=', auth()->id())->paginate(15);
               return view('consultas.misConsultas')
               ->with('consultas', $consultas); 
    }

        public function guardarComentario(Request $request, $id)
    {
        $c = new Comconsultas();
        $c->id_consulta = $id;
        $c->nombre = $request->nombre;
        $c->email = $request->email;
        $c->comentario = $request->comentario;

       if($c->save()){
            return redirect()->route('consultas.misConsultas')->with('success-message', 'Consulta enviada con éxito.');
        }else{
            return redirect('uploads')->with('error-message', 'An error ocurred saving data into database');
        } 
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $consultas= Consultas::find($id);
                
        return view('consultas.edit')->with('consultas', $consultas);
    }

    public function administracion()
    {
   $consultas = Consultas::orderBy('id', 'DESC')
   ->paginate(15);
               return view('consultas.administracion')
               ->with('consultas', $consultas); 
    
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $consultas= Consultas::find($id)->update($request->all());

        return redirect()->route('consultas.administracion')->with('success','Registro actualizado satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            Consultas::find($id)->delete();
        return redirect()->route('consultas.administracion')->with('success','Registro eliminado satisfactoriamente');
    }

        public function presentacion()
    {
        $id = 4;
       $contenido=Contenidos::find($id);        
        return view('consultas.presentacion')->with ('contenido', $contenido);
    }

    public function updatePresentacion(Request $request, $id)
    {
        $this->validate($request,[ 'contenido'=>'required']);

       Contenidos::find($id)->update($request->all());


        return redirect()->route('consultas.administracion')->with('success','Registro actualizado satisfactoriamente');
    }
}
