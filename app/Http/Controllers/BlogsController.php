<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blogs;
use DB;
use App\ComentarioBlog;
use Input;
use Image; 
use MimeTypeGuesser;
use App\Contenidos;


class BlogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog=Blogs::orderBy('created_at', 'DESC')->paginate(15);
        
        return view('blog') 
        ->with('blog', $blog);
    }

        public function blogIndex()
    {
        $blog=Blogs::orderBy('id')->paginate(15);
        return view('blog',compact('blog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        // dd(request()->video);

   // dd(request()->video->getMimeType());
    //$mime = \Input::file('imagen')->getMimeType();
   // $extension = strtolower(\Input::file('imagen')->getClientOriginalExtension());
    //$fileName = uniqid().'.'.$extension;
    //$path = "files_uploaded";
 
    //switch ($mime)
    //{
       /* case "image/jpeg":
        case "image/png":
        case "image/gif":
        case "application/pdf":
        case "application/pptx":
        case "application/docx":
        case "application/xlsx":
        case "video/mp4":*/
            /*if (\Request::file('imagen')->isValid())
            {*/
               // $pathPDF = "C:/Users/Ernesto/APP_U/public/";
                $blog = new Blogs();
         if (($request->blogImagen) != NULL)
            {
        $blog->imagen =$request->file('blogImagen')->store('public');
    }       
        $blog->titulo = $request->input('titulo'); 
        $blog->contenido = $request->input('contenido'); 
        /*$blog->imagen =$request->file('imagen')->store('public');*/

    if (($request->archivopdf) != NULL) {
        $extension=$request->archivopdf ->getClientOriginalExtension();
        if($extension != 'pdf'){
            return redirect()->route('blog.index')->with('success-message', 'Error en el documento PDF');
        }else{
            $archivopdf =$request->file('archivopdf')->store('public/store');
        $blog->archivo = $archivopdf;
        }
                
    }if (($request->Archivoppt) != NULL) {
        /*$extension=$request->Archivoppt ->getClientOriginalExtension();
        if($extension != 'ppt' || $extension != 'pptx'){
            return redirect()->route('blog.index')->with('success-message', 'Error en el documento PPT');
        }else{*/
            $Archivoppt =$request->file('Archivoppt')->store('public/store');
        $blog->ppt = $Archivoppt;
        //}

    }if (($request->archivodocx) != NULL) {
        /*$extension=$request->archivodocx ->getClientOriginalExtension();
        if($extension != 'doc' || $extension != 'docx'){
            return redirect()->route('blog.index')->with('success-message', 'Error en el documento Word');
        }else{*/
        $archivodocx =$request->file('archivodocx')->store('public/store');
        $blog->docx = $archivodocx;
        //}
   }if (($request->archivoxlsx) != NULL) {
        /*$extension=$request->archivoxlsx ->getClientOriginalExtension();
        if($extension != 'xls' || $extension != 'xlsx'){
            return redirect()->route('blog.index')->with('success-message', 'Error en el documento Excel');
        }else{*/
        $archivoxlsx =$request->file('archivoxlsx')->store('public/store');
        $blog->xlsx = $archivoxlsx;
        //}
    }if (($request->archivovideo) != NULL) {
        $extension=$request->archivovideo ->getClientOriginalExtension();
        if($extension != 'mp4' || $extension != 'mp4'){
            return redirect()->route('blog.index')->with('success-message', 'Error en el video');
        }else{
        $archivovideo =$request->file('archivovideo')->store('public/store');
        $blog->video = $archivovideo;
        }
    }

        //$blog->archivo = $pathPDF.$request->file('archivo')->getClientOriginalName();
        //$pdf = PDF::loadView($blog->archivo);
                if($blog->save())
                {
                    return redirect()->route('blog.index')->with('success-message', 'File has been uploaded');
                }
                else
                {
                    \File::delete($path."/".$fileName);
                    return redirect('uploads')->with('error-message', 'An error ocurred saving data into database');
                }
            
        //break;
        /*default:
            return redirect('uploads')->with('error-message', 'Extension file is not valid');*/
    
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog=Blogs::find($id);
        return  view('blog',compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog=Blogs::find($id);
        $blogcomen = ComentarioBlog::where('id_blog','=',$id)->paginate(10); 
        
        return view('blog.editar') 
        ->with('blog', $blog)
        ->with('blogcomen', $blogcomen);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
 
        $blog= Blogs::find($id)->update($request->all());

        $blog->update($request->only('titulo', 'contenido'));


        return redirect()->route('blog.index')->with('success','Registro actualizado satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Blogs::find($id)->delete();
        return redirect()->route('blog.index')->with('success','Registro eliminado satisfactoriamente');
    }
        public function presentacion()
    {
        $id = 3;
       $blog=Contenidos::find($id);        
        return view('blog.presentacion')->with ('blog', $blog);
    }

    public function updatePresentacion(Request $request, $id)
    {
        $this->validate($request,[ 'contenido'=>'required']);

       Contenidos::find($id)->update($request->all());


        return redirect()->route('blog.index')->with('success','Registro actualizado satisfactoriamente');
    }

}
