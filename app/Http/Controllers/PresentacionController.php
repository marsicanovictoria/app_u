<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Presentacion;
use App\PresComentario;

class PresentacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $presentacion=Presentacion::orderBy('id')->paginate(3);
        $presComentario=PresComentario::orderBy('id')->paginate(10);
        return view('presentacion')
        ->with('presentacion', $presentacion)
        ->with('presComentario', $presComentario);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('presentacion.presentacioncrear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[ 'pensamiento'=>'required', 'presentacion'=>'required', 'proposito'=>'required', 'proposito2'=>'required', 'proposito3'=>'required']);
        Presentacion::create($request->all());

        return redirect()->route('presentacion')->with('success','Registro creado satisfactoriamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $presentacion=Presentacion::find($id);
        return  view('presentacion',compact('presentacion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $presentacion=Presentacion::find($id);
        return view('presentacion.presentacioneditar',compact('presentacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[ 'pensamiento'=>'required', 'presentacion'=>'required', 'proposito'=>'required', 'proposito2'=>'required', 'proposito3'=>'required']);
 
        Presentacion::find($id)->update($request->all());
        return redirect()->route('presentacion.index')->with('success','Registro actualizado satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PresComentario::find($id)->delete();
        return redirect()->route('presentacion.index')->with('success','Registro eliminado satisfactoriamente');
    }
    public function storecom($id)
    {
        $this->validate($request,[ 'comentarios'=>'required']);
        PresComentario::create($request->all());

        return redirect()->route('presentacion')->with('success','Registro creado satisfactoriamente');
    }

        public function storeComentario(Request $request)
    {
        $this->validate($request,[ 'comentarios'=>'required']);
 
        PresComentario::create($request->all());

        return redirect()->with('success','Registro creado satisfactoriamente');
    }

}
