<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comunicacion;
use App\Ejes;
use App\ComenComunicacion;
use DB;
use App\Contenidos;

class ComunicacionIndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = 1;
       $comunicacion=Contenidos::find($id);  
        return view('comunicacionPresentacion')
        ->with('comunicacion', $comunicacion);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    $comunicacion = Comunicacion::where('id_usuario','=', auth()->id())
    ->orderBy('id', 'DESC')
    ->paginate(10);
               return view('comunicacionAporte')
               ->with('comunicacion', $comunicacion);  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $comen = new ComenComunicacion();
        $comen->id_comunicacion = $id;
        $comen->comentario = $request->comentarios;
        $comen->nombre = $request->nombre;
        $comen->email = $request->email;

        $success = $comen->save() ? $request->session()->flash('success', '¡Registro exitoso!') : $request->session()->flash('success', 'Ooops! Algo salio mal :(');

        
        $comunicacion=Comunicacion::orderBy('id')->paginate(15);
        return view('comunicacionIndex') ->with('comunicacion', $comunicacion);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $titulo = $request->titulo;
        $comunicacion=Comunicacion::where('titulo', 'LIKE', "%{$titulo}%" )->get();
        return  view('comunicacionIndex', compact('comunicacion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comunicacion=Comunicacion::find($id);

        //$comen = ComentarioBlog::where('id_blog','=',$id)->paginate(10); 

        //$path= "C:/Users/Ernesto/APP_U/storage/app/";
       // $file= $path.$blog->video;  
        //$info = LaravelVideoEmbed::parse($file);

        return view('comunicacionContent') 
        ->with('comunicacion', $comunicacion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[ 'titulo'=>'required', 'comunicado'=>'required', 'eje'=>'required']);

       Comunicacion::find($id)->update($request->all());


        return redirect()->route('comunicacionIndex.create')->with('success','Registro actualizado satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $c= ComenComunicacion::orderBy('id', 'DESC')->get();
            $comunicacion = DB::table('comunicacions')
        ->join ('users', 'comunicacions.id_usuario', '=', 'users.id')
        ->select('comunicacions.*', 'users.name')
        ->orderBy('id', 'DESC')
        ->get();
    return view('comunicacionIndex')->with ('comunicacion', $comunicacion)
    ->with ('c', $c);
    }

    public function content($id)
    {
                     
        $comunicacion=Comunicacion::find($id);
        $eje = Ejes::all('id', 'descripcion')->pluck('descripcion', 'descripcion');
        
        return view('comunicacionEditar') 
        ->with('comunicacion', $comunicacion)
        ->with('eje', $eje);
    }

    public function all_comunicaciones($id)
    {
            /*$comunicacion = Comunicacion::orderBy('id', 'DESC')->paginate(15);
    return view('comunicacionIndex')->with ('comunicacion', $comunicacion);
*/
        $c= ComenComunicacion::orderBy('id', 'DESC')->get();
        $comunicacion = DB::table('comunicacions')
        ->join ('users', 'comunicacions.id_usuario', '=', 'users.id')
        ->select('comunicacions.*', 'users.name')
        ->get();
    return view('comunicacionIndex')->with ('comunicacion', $comunicacion)
    ->with ('c', $c);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function administracion()
    {
    $comen = ComenComunicacion::orderBy('id', 'DESC')
    ->paginate(15);
               return view('comunicacion.administracion')
               ->with('comen', $comen);  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminarComentario($id)
    {
        ComenComunicacion::find($id)->delete();
    }

    public function verComentario($id){

        $comen=ComenComunicacion::find($id);
    return view('comunicacion.comentario')->with('comen', $comen);  
    }

}
