<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Khill\Lavacharts\Lavacharts;
use App\Http\Controllers\Auth\ResetPasswordController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class AdministracionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    $users = User::orderBy('id','DESC')->paginate(15);
$lava = new Lavacharts; // See note below for Laravel

$population = $lava->DataTable();

$population->addDateColumn('Date')
           ->addNumberColumn('Max Temp')
             ->addNumberColumn('Mean Temp')
             ->addNumberColumn('Min Temp')
           ->addRow(['2019-01-01', 3])
           ->addRow(['2018-12-01', 3]);

$lava->LineChart('population', $population, [
    'title' => 'population Growth',
    'legend' => [
        'position' => 'in'
    ]
]);
    return view('administracion')
    ->with('lava', $lava)
    ->with('users',$users);
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id)
    {
        $usuario= User::find($id);
        $usuario->activo = 0;
        if($usuario->save()){
        return redirect()->route('administracion.index')->with('success','Usuario suspendido satisfactoriamente');
        }else{
             return redirect('uploads')->with('error-message', 'An error ocurred saving data into database');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users=User::find($id);
        return  view('user.show',compact('users')->paginate(15));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
           {
        $users=User::find($id);
        return view('usuario.editar', compact('users'));
    }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
 
        $usuario= User::find($id);
        $usuario->name = $request->name;
        $usuario->admin = $request->optradio;
        if($usuario->save()){
        return redirect()->route('administracion.index')->with('success','Registro actualizado satisfactoriamente');
        }else{
             return redirect('uploads')->with('error-message', 'An error ocurred saving data into database');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('administracion.index')->with('success','Registro eliminado satisfactoriamente');
    }
public function count($id){
    $users=User::count();
return $users;
}

    public function chartjs()
{

//
}

    public function postReset(Request $request, $id)
    {
        $credentials = [
            'id' => $request->id,
            'email' => $request->email,
            'password' => $request->password,
            'password_confirmation' => $request->passwordconfirm
        ];

        //dd($request);
                $response = Password::reset($cred, function ($user, $password) {
            $request->password = Hash::make($request->password);
            $request->save();
            return view('configuracion.administracion');
        });       
        return view('configuracion.administracion'); 
    }

}
