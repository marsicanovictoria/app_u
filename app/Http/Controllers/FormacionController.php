<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Area;
use App\Formacion;
use App\ComentarioFormacion;
use App\Contenidos;

class FormacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
                 $id = 2;
       $contenido=Contenidos::find($id); 
        $area = Area::all('id', 'area')->pluck('area', 'area');
                return view('formacion.index')
         ->with('area', $area)
         ->with('contenido', $contenido);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $area = Area::all('id', 'area')->pluck('area', 'area');
        return view('formacion.create')
         ->with('area', $area);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formacion = new Formacion();
        $formacion->convocatoria = $request->convocatoria;
        $formacion->requisito = $request->requisitos;
        $formacion->area = $request->area;
        $formacion->fecha_ini = $request->fecha1;
        $formacion->id_usuario = auth()->id();
        $formacion->fecha_fin = $request->fecha2;
        $formacion->lugar = $request->lugar;
        $formacion->responsable = $request->responsable;

        $request->validate([

            'captcha' => 'required|captcha'
        ]);


        
        if (($request->programaImg) != NULL)
            {
        $formacion->imagen =$request->file('programaImg')->store('public');
    }elseif (($request->programapdf) != NULL) {
        $extension=$request->programapdf ->getClientOriginalExtension();
        if($extension != 'pdf'){
            return redirect()->route('formacion.create')->with('success-message', 'Error en el documento PDF');
        }else{
            $programapdf =$request->file('programapdf')->store('public/store');
        $formacion->pdf = $programapdf;
        }
                
    }elseif (($request->programappt) != NULL) {
        $extension=$request->programappt ->getClientOriginalExtension();
        if($extension != 'ppt'){
            return redirect()->route('formacion.create')->with('success-message', 'Error en el documento PPT');
        }else{
            $programappt =$request->file('programappt')->store('public/store');
        $formacion->ppt = $programappt;
        }

    }elseif (($request->programapdfword) != NULL) {
        $extension=$request->programapdfword ->getClientOriginalExtension();
        if($extension != 'doc' || $extension != 'docx'){
            return redirect()->route('formacion.create')->with('success-message', 'Error en el documento Word');
        }else{
        $programapdfword =$request->file('programapdfword')->store('public/store');
        $formacion->doc = $programapdfword;
        }
    }elseif (($request->programapdfexcel) != NULL) {
        $extension=$request->programapdfexcel ->getClientOriginalExtension();
        if($extension != 'xls' || $extension != 'xlsx'){
            return redirect()->route('formacion.create')->with('success-message', 'Error en el documento Excel');
        }else{
        $programapdfexcel =$request->file('programapdfexcel')->store('public/store');
        $formacion->excel = $programapdfexcel;
        }
    }elseif (($request->programapdfvideo) != NULL) {
        $extension=$request->programapdfvideo ->getClientOriginalExtension();
        if($extension != 'mp4' || $extension != 'mp4'){
            return redirect()->route('formacion.create')->with('success-message', 'Error en el video');
        }else{
        $programapdfvideo =$request->file('programapdfvideo')->store('public/store');
        $formacion->video = $programapdfvideo;
        }
    }

        if($formacion->save()){
            return redirect()->route('formacion.programa')->with('success-message', 'File has been uploaded');
        }else{
            return redirect('uploads')->with('error-message', 'An error ocurred saving data into database');
        }       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

        /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        $area = $request->area;
        $convocatorias=Formacion::where('area', 'LIKE', "%{$area}%" )->get();

        $area = Area::all('id', 'area')->pluck('area', 'area');
        return view('formacion.all')
         ->with('area', $area)
         ->with('convocatorias', $convocatorias);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $programa = Formacion::find($id);
         $comen = ComentarioFormacion::where('id_formacion','=',$id)->paginate(10);
        $area = Area::all('id', 'area')->pluck('area', 'area');
        return view('formacion.edit')->with('programa', $programa)
        ->with('comen', $comen)
        ->with('area', $area);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[ 'convocatoria'=>'required', 'requisito'=>'required', 'area'=>'required', 'fecha1'=>'required', 'fecha2'=>'required','lugar'=>'required', 'responsable'=>'required']);

       Formacion::find($id)->update($request->all());


        return redirect()->route('formacion.programa')->with('success','Registro actualizado satisfactoriamente'); //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Formacion::find($id)->delete();
        return redirect()->route('formacion.programa')->with('success','Registro eliminado satisfactoriamente');
    }

        /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function administracion()
    {
        $programa = Formacion::orderBy('id', 'DESC')
    ->paginate(15);
        return view('formacion.administracion')->with('programa', $programa);
    }

        /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function aporte()
    {
        $programa = Formacion::orderBy('id', 'DESC')->paginate(15);
        return view('formacion.administracion')->with('programa', $programa);
    }

    public function pdf($id){
        $programa=Formacion::find($id);
        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $file= $path.$programa->pdf;
        $headers = array(
              'Content-Type: application/pdf',
            );

    return Response::download($file, 'archivo.pdf', $headers);
    }

        public function ppt($id){
        $programa=Formacion::find($id);
        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $file= $path.$noticias->ppt;
        $headers = array(
              'Content-Type: application/ppt',
            );

    return Response::download($file, 'archivo.ppt', $headers);
    }

    public function word($id){
        $programa=Formacion::find($id);
        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $file= $path.$noticias->word;
        $headers = array(
              'Content-Type: application/docx',
            );

    return Response::download($file, 'archivo.docx', $headers);
    }

    public function excel($id){
        $programa=Formacion::find($id);
        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $file= $path.$noticias->excel;
        $headers = array(
              'Content-Type: application/xlsx',
            );

    return Response::download($file, 'archivo.xlsx', $headers);
    }

    public function video($id){
        $programa=Formacion::find($id);
        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $file= $path.$noticias->video;
        $headers = array(
              'Content-Type: application/mp4',
            );

    return Response::download($file, 'archivo.mp4', $headers);
    }

    public function comentario(Request $request, $id){

        $comentario = new ComentarioFormacion();
        $comentario->nombre = $request->nombre;
        $comentario->apellido = $request->apellido;
        $comentario->mail = $request->mail;
        $comentario->comentario = $request->comentario;
        $comentario->id_formacion = $request->id;

        $area = Area::all('id', 'area')->pluck('area', 'area');

        $success = $comentario->save() ? $request->session()->flash('success', '¡Registro exitoso!') : $request->session()->flash('success', 'Ooops! Algo salio mal :(');     
        return view('formacion.index')-> with('area', $area);
    }

    public function area()
    {
        
        return view('formacion.areas');
    }

public function persona()
    {
        $convocatorias=Formacion::where('area', 'LIKE', 
            "%La Persona%" )
        ->orderBy('id', 'DESC')
        ->get();
        return view('formacion.all')->with('convocatorias', $convocatorias);
    }

    public function contexto()
    {
        $convocatorias=Formacion::where('area', 'LIKE', 
            "%El contexto%" )
        ->orderBy('id', 'DESC')
        ->get();
        return view('formacion.all')->with('convocatorias', $convocatorias);
    }

    public function conocimiento()
    {
        $convocatorias=Formacion::where('area', 'LIKE', 
            "%Sociedad del conocimiento%" )
        ->orderBy('id', 'DESC')
        ->get();
        return view('formacion.all')->with('convocatorias', $convocatorias);
    }

        public function tecnologia()
    {
        $convocatorias=Formacion::where('area', 'LIKE', 
            "%La ciencia y la tecnología%" )
        ->orderBy('id', 'DESC')
        ->get();
        return view('formacion.all')->with('convocatorias', $convocatorias);
    }

        public function empresa()
    {
        $convocatorias=Formacion::where('area', 'LIKE', 
            "%La empresa%" )
        ->orderBy('id', 'DESC')
        ->get();
        return view('formacion.all')->with('convocatorias', $convocatorias);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function convocatoria()
    {
        
        return view('formacion.nueva');
    }

            public function presentacion()
    {
        $id = 2;
       $formacion=Contenidos::find($id);        
        return view('formacion.presentacion')->with ('formacion', $formacion);
    }

    public function updatePresentacion(Request $request, $id)
    {
        $this->validate($request,[ 'contenido'=>'required']);

       Contenidos::find($id)->update($request->all());


        return redirect()->route('formacion.administracion')->with('success','Registro actualizado satisfactoriamente');
    }

}
