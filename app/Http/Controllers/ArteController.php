<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Arte;
use App\Clasificacion;
use Response;
use App\Contenidos;


class ArteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arte = Arte::orderBy('id', 'DESC')->paginate(15);
$id = 5;
       $contenido=Contenidos::find($id); 
        return view('arte.index')
               ->with('arte', $arte)
               ->with('contenido', $contenido);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clasificacion = Clasificacion::all('id', 'descripcion')->pluck('descripcion', 'descripcion');
        return view('arte.create', compact('clasificacion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $arte = new Arte();
        $arte->titulo = $request->titulo;
        $arte->contenido = $request->contenido;
        $arte->id_usuario = auth()->id();
        $arte->clasificacion = $request->clasificacion;

        $request->validate([

            'captcha' => 'required|captcha'
        ]);


        
        if (($request->arteImagen) != NULL)
            {
        $arte->imagen =$request->file('arteImagen')->store('public');
    }if (($request->artePdf) != NULL) {
                $artepdf =$request->file('artePdf')->store('public/store');
        $arte->pdf = $artepdf;
    }if (($request->artePpt) != NULL) {
        $arteppt =$request->file('artePpt')->store('public/store');
        $arte->ppt = $arteppt;
    }if (($request->arteWord) != NULL) {
        
        $arteword =$request->file('arteWord')->store('public/store');
        $arte->word = $arteword;
    }if (($request->arteExcel) != NULL) {
        $arteexcel =$request->file('arteExcel')->store('public/store');
        $arte->excel = $arteexcel;
    }if (($request->arteVideo) != NULL) {
        $artevideo =$request->file('arteVideo')->store('public/store');
        $arte->video = $artevideo;
    }
//dd($request);
        if($arte->save()){
            return redirect()->route('arte.index')->with('success-message', 'File has been uploaded');
        }else{
            return redirect('uploads')->with('error-message', 'An error ocurred saving data into database');
        }     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $arte= Arte::find($id);
        return view('arte.content')->with('arte', $arte);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arte= Arte::find($id);
        $clasificacion = Clasificacion::all('id', 'descripcion')->pluck('descripcion', 'descripcion');

        return view('arte.edit')->with('arte', $arte)
        ->with('clasificacion', $clasificacion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateArte(Request $request, $id)
    {
        $this->validate($request,[ 'titulo'=>'required', 'contenido'=>'required', 'clasificacion'=>'required']);

       Arte::find($id)->update($request->all());


        return redirect()->route('arte.index')->with('success','Registro actualizado satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Arte::find($id)->delete();
        return redirect()->route('arte.index')->with('success','Registro eliminado satisfactoriamente');
    }

        public function presentacion()
    {
        $id = 5;
       $contenido=Contenidos::find($id); 
        return view('arte.presentacion')
        ->with('contenido', $contenido);
    }

        public function misAportes()
    {
        
        $arte = Arte::orderBy('id', 'DESC')
            ->where('id_usuario','=', auth()->id())->paginate(15);
               return view('arte.misAportes')
               ->with('arte', $arte); 
    }


    public function ShowMusica()
    {
        $arte=Arte::orderBy('id', 'DESC')
        ->where('clasificacion', 'LIKE', '%Musica%' )->get();

        return view('arte.all')
               ->with('arte', $arte); 

       /* $arte=Arte::where('clasificacion', 'LIKE', "%{Musica}%" )->get();
               return view('arte.all')
               ->with('arte', $arte); */
    }

    public function ShowDanza()
    {
        $arte=Arte::orderBy('id', 'DESC')
        ->where('clasificacion', 'LIKE', '%Danza%' )->get();

        return view('arte.all')
               ->with('arte', $arte); 

       /* $arte=Arte::where('clasificacion', 'LIKE', "%{Musica}%" )->get();
               return view('arte.all')
               ->with('arte', $arte); */
    }

        public function ShowPintura()
    {
        $arte=Arte::orderBy('id', 'DESC')
        ->where('clasificacion', 'LIKE', '%Pintura%' )->get();

        return view('arte.all')
               ->with('arte', $arte); 

       /* $arte=Arte::where('clasificacion', 'LIKE', "%{Musica}%" )->get();
               return view('arte.all')
               ->with('arte', $arte); */
    }

        public function ShowOtros()
    {
        $arte=Arte::orderBy('id', 'DESC')
        ->where('clasificacion', 'LIKE', '%Pensamientos, Dichos, Refranes%' )->get();

        return view('arte.all')
               ->with('arte', $arte); 

       /* $arte=Arte::where('clasificacion', 'LIKE', "%{Musica}%" )->get();
               return view('arte.all')
               ->with('arte', $arte); */
    }

public function pdf($id){
        $arte=Arte::find($id);
        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $file= $path.$arte->pdf;
        $headers = array(
              'Content-Type: application/pdf',
            );

    return Response::download($file, 'archivo.pdf', $headers);
    }

        public function ppt($id){
        $arte=Arte::find($id);
        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $file= $path.$arte->ppt;
        $headers = array(
              'Content-Type: application/ppt',
            );

    return Response::download($file, 'archivo.ppt', $headers);
    }

    public function word($id){
        $arte=Arte::find($id);
        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $file= $path.$arte->word;
        $headers = array(
              'Content-Type: application/docx',
            );

    return Response::download($file, 'archivo.docx', $headers);
    }

    public function excel($id){
        $arte=Arte::find($id);
        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $file= $path.$arte->excel;
        $headers = array(
              'Content-Type: application/xlsx',
            );

    return Response::download($file, 'archivo.xlsx', $headers);
    }

    public function video($id){
        $arte=Arte::find($id);
        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $file= $path.$arte->video;
        $headers = array(
              'Content-Type: application/mp4',
            );

    return Response::download($file, 'archivo.mp4', $headers);
    }

            public function ShowChistes()
    {
        $arte=Arte::orderBy('id', 'DESC')
        ->where('clasificacion', 'LIKE', '%Amenidades y Chistes%' )->get();

        return view('arte.all')
               ->with('arte', $arte); 

       /* $arte=Arte::where('clasificacion', 'LIKE', "%{Musica}%" )->get();
               return view('arte.all')
               ->with('arte', $arte); */
    }

        public function congifpresentacion()
    {
        $id = 5;
       $arte=Contenidos::find($id);        
        return view('arte.config')->with ('arte', $arte);
    }

    public function updatePresentacion(Request $request, $id)
    {
        $this->validate($request,[ 'contenido'=>'required']);

       Contenidos::find($id)->update($request->all());


        return redirect()->route('arte.administracion')->with('success','Registro actualizado satisfactoriamente');
    }
}
