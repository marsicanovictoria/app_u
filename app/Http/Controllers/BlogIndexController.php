<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ComentarioBlog;
use App\Blogs;
use DB;
use Barryvdh\DomPDF\Facade as PDF;
use Response;
use LaravelVideoEmbed;
use Input;
use App\Contenidos;

class BlogIndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $id = 3;
       $contenido=Contenidos::find($id);  
        $blog=Blogs::orderBy('id', 'DESC')->paginate(15);
        return view('blogIndex') ->with('blog', $blog)
        ->with('contenido', $contenido);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {

        $comen = new ComentarioBlog();
        $comen->id_blog = $id;
        $comen->comentario = $request->comentarios;
        $comen->nombre = $request->nombre;
        $comen->email = $request->email;

        $success = $comen->save() ? $request->session()->flash('success', '¡Registro exitoso!') : $request->session()->flash('success', 'Ooops! Algo salio mal :(');

        $blog=Blogs::orderBy('id')->paginate(15);
        return view('blogIndex') ->with('blog', $blog);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $titulo = $request->titulo;
        $blog=Blogs::where('titulo', 'LIKE', "%{$titulo}%" )->get();
        return  view('blogIndex', compact('blog'));

        /*        $blog=Blogs::where('titulo', '=', $request->titulo)->paginate();
        return  view('blogIndex', compact('blog'));*/
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $blog=Blogs::find($id);

        $comen = ComentarioBlog::where('id_blog','=',$id)->paginate(10); 

        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $file= $path.$blog->video;  
        //$info = LaravelVideoEmbed::parse($file);


        return view('blogContent') 
        ->with('blog', $blog)
        ->with('comen', $comen);
        //->with('info', $info);
    }

        public function pdf($id)
    {        
        /**
         * toma en cuenta que para ver los mismos 
         * datos debemos hacer la misma consulta
        **/
        $blog=Blogs::find($id);
        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $file= $path.$blog->archivo;
        $headers = array(
              'Content-Type: application/pdf',
            );
        

        return Response::download($file, 'archivo.pdf', $headers);
    }

            public function ppt($id)
    {        
        /**
         * toma en cuenta que para ver los mismos 
         * datos debemos hacer la misma consulta
        **/
        $blog=Blogs::find($id);
        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $file= $path.$blog->ppt;
        $headers = array(
              'Content-Type: application/pptx',
            );
        

        return Response::download($file, 'archivo.pptx', $headers);
    }


         public function docx($id)
    {        
        /**
         * toma en cuenta que para ver los mismos 
         * datos debemos hacer la misma consulta
        **/
        $blog=Blogs::find($id);
        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $file= $path.$blog->docx;
        $headers = array(
              'Content-Type: application/docx',
            );
        

        return Response::download($file, 'archivo.docx', $headers);
    }

         public function xlsx($id)
    {        
        /**
         * toma en cuenta que para ver los mismos 
         * datos debemos hacer la misma consulta
        **/
        $blog=Blogs::find($id);
        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $file= $path.$blog->xlsx;
        $headers = array(
              'Content-Type: application/xlsx',
            );

        return Response::download($file, 'archivo.xlsx', $headers);

    }


         public function video($id)
    {        
        /**
         * toma en cuenta que para ver los mismos 
         * datos debemos hacer la misma consulta
        **/
        $blog=Blogs::find($id);
        $path= "C:/Users/Ernesto/APP_U/storage/app/";
        $file= $path.$blog->video;   

        //$stream = new \App\Http\VideoStream($file);

        /*return response()->stream(function() use ($stream) {
            $stream->start();
        });*/

        //return \View::make('blogContent', compact('file'));
        $headers = array(
              'Content-Type: video/mp4',
            );

        return Response::download($file, 'archivo.mp4', $headers);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ComentarioBlog::find($id)->delete();
        return redirect()->route('blog.index')->with('success','Registro eliminado satisfactoriamente');
    }
}
