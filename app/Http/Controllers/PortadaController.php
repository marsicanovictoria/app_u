<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Portada;
use Input;

class PortadaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portadas=Portada::orderBy('id')->paginate(3);
            return view('portada')->with('portadas', $portadas);
        }
        
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $portadas=Portada::find($id);
        return  view('portada',compact('portadas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $portadas=Portada::find($id);
        return view('portada.editar',compact('portadas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $file = Input::file('imagen');
        $imagen = \Image::make (\Input::file('imagen'));

        $path = public_path().'/img/';

        $imagen -> save($path.$file ->getClientOriginalName());

        $this->validate($request,[  'imagen'=>'required']);
        $portada =  new Portada();

        //Portada::find($id)->update($request->all());
        return redirect()->route('portada.index')->with('success','Registro actualizado satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
