<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PresComentario extends Model
{  
     protected $fillable = ['id', 'comentarios', 'nombre', 'email'];
}
