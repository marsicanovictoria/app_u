<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Foro extends Model
{
         protected $fillable = [
        'id', 'eje', 'contenido', 'activo', 'id_usuario', 'titulo', 'mensaje', 'visto','foro', 'fecha_fin'
    ];
}
