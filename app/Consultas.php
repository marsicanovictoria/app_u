<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consultas extends Model
{
        protected $fillable = [
        'nombre', 'pseudonimo', 'email',
        'telefono', 'consulta', 'respuesta', 'id_usu_con', 'id_usu_res', 'name_usu_res', 'activo', 'created_at'
    ];

    protected $table = 'consultas';
}
