<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComentarioArte extends Model
{
    protected $fillable['id', 'id_usuario', 'comentario'];
}
