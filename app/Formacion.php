<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formacion extends Model
{
   protected $fillable = [
        'id','area','convocatoria','requisito','lugar','responsable','pdf','doc','excel','ppt','video','imagen','fecha_ini','fecha_fin'
    ];
}
