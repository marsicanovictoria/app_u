<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Arte extends Model
{
    protected $fillable= ['id', 'titulo', 'contenido', 'clasificacion', 'id_usuario', 'imagen', 'pdf', 'word', 'excel', 'ppt', 'video'];
}
