<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blogs extends Model
{
      protected $fillable = [
        'id','titulo', 'contenido', 'imagen', 'archivo', 'ppt', 'docx', 'xlsx', 'video'
    ];
}
