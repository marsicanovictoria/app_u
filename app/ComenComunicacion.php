<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComenComunicacion extends Model
{
      protected $fillable = [
        'id','nombre', 'email', 'comentario', 'id_comunicacion'
    ];

    protected $table = 'comen_comunicacions';
}
