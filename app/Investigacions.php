<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investigacions extends Model
{
    protected $fillable = ['id', 'titulo', 'descripcion', 'documento', 'id_usuario', 'invpdf', 'estado'];
}
