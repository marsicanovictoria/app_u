<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contenidos extends Model
{
     protected $fillable = ['portal', 'contenido'];
}
