<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Identificacion extends Model
{
    protected $fillable = ['id', 'tipo_identificacion'];
}
