<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComentarioBlog extends Model
{
  protected $fillable = [
        'id','id_blog', 'comentario', 'nombre', 'email'
    ];


    protected $table = 'comentario_blogs';
}
