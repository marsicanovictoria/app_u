<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComentarioNoticia extends Model
{
            protected $fillable = [
        'id', 'nombre', 'apellido', 'email', 'comentario'
    ];
protected $table = 'comentarionoticia';
}
