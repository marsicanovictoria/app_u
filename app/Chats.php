<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chats extends Model
{
          protected $fillable = [
        'id','id_usuario', 'mensaje', 'visto', 'titulo', 'descripcion'
    ];
}
