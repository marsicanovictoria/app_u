<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comconsultas extends Model
{
    protected $fillable=['id', 'nombre', 'email', 'comentario', 'id_consulta'];

    protected $table = 'comconsultas';
}
