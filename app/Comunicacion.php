<?php

namespace App;
 
use Illuminate\Database\Eloquent\Model;

class Comunicacion extends Model
{
        protected $fillable = [
        'titulo', 'comunicado', 'eje', 'id_usuario'
    ];
}
