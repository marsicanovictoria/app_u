<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Noticias extends Model
{
    protected $fillable=['id', 'titulo', 'contenido', 'entidad', 'carrera', 'imagen', 'created_at', 'pdf', 'word', 'ppt', 'video', 'excel', 'tipo_identificacion', 'numero_identificacion', 'nombre', 'email'];
}
