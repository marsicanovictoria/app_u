<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cominvs extends Model
{
        protected $fillable = [
        'id', 'nombre', 'apellido', 'email', 'comentario', 'id_inv'
    ];

    protected $table = 'cominvs';
}
