<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Presentacion extends Model
{
    protected $fillable = ['pensamiento', 'presentacion', 'proposito','proposito2','proposito3'];
}
